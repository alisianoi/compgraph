#pragma once

#include "filterop.h"

class ThresholdOp final
{
    mutable SDMatrix _map;
    double _thresh1;
    double _thresh2;
 public:
    ThresholdOp(const SDMatrix &G, double, double);
    
    virtual ~ThresholdOp() {};

    virtual Image operator() ();
};
