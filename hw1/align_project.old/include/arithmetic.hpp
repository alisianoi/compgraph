#pragma once

#include <cmath> // for atan2

template<typename T>
class ApplyBinMapOp
{
    std::size_t _radius;
    std::size_t _radius_i;
    std::size_t _radius_j;
public:
    ApplyBinMapOp() : _radius(1), _radius_i(1), _radius_j(1) {};

    virtual ~ApplyBinMapOp() {};

    virtual std::tuple<uint, uint, uint>
    operator() (const Matrix<std::tuple<T, T, T>> &,
                const Matrix<std::tuple<T, T, T>> &) const;
};

template<typename T>
class MaxGradMapOp
{
    std::size_t _radius;
    std::size_t _radius_i;
    std::size_t _radius_j;
public:
    MaxGradMapOp() : _radius(1), _radius_i(1), _radius_j(1) {};

    virtual ~MaxGradMapOp() {};
    
    virtual std::tuple<T, T, T>
    operator() (const Matrix<std::tuple<T, T, T>> &grads,
                const Matrix<std::tuple<T, T, T>> &thetas) const;
    
    auto getRadius() const -> decltype(_radius)
    {
        if (_radius_i != _radius_j) {
            throw "MaxGradMapOp with non-square filter";
        }

        return _radius;
    }

    auto getRadiusi() const -> decltype(_radius_i) {return _radius_i;}
    auto getRadiusj() const -> decltype(_radius_j) {return _radius_j;}
};

template<typename T>
std::tuple<T, T, T>
MaxGradMapOp<T>::operator() (const Matrix<std::tuple<T, T, T>> &grads,
                             const Matrix<std::tuple<T, T, T>> &thetas) const
{
    // assume thetas for different channels of the same pixel match.
    // also assume radius == 1 => 3 x 3 matrices...
    T theta = std::get<0>(thetas(1, 1));
    T grad1, grad2, grad = std::get<0>(grads(1, 1));
    if ((-M_PI / 8 < theta && theta < M_PI / 8)
        || theta > 7 * M_PI / 8 || theta < -7 * M_PI / 8) {
        grad1 = std::get<0>(grads(1, 2));
        grad2 = std::get<0>(grads(1, 0));
        if (!(grad > grad1 && grad > grad2)) {
            return std::make_tuple(0, 0, 0);
        }
    } else if ((M_PI / 8 < theta && theta < 3 * M_PI / 8)
               || (-7 * M_PI / 8 < theta && theta < -5 * M_PI / 8)) {
        grad1 = std::get<0>(grads(0, 2));
        grad2 = std::get<0>(grads(2, 0));
        if (!(grad > grad1 && grad > grad2)) {
            return std::make_tuple(0, 0, 0);
        }
    } else if ((3 * M_PI / 8 < theta && theta < 5 * M_PI / 8)
               || (-5 * M_PI / 8 < theta && theta < -3 * M_PI / 8)) {
        grad1 = std::get<0>(grads(0, 1));
        grad2 = std::get<0>(grads(2, 1));
        if (!(grad > grad1 && grad > grad2)) {
            return std::make_tuple(0, 0, 0);
        }
    } else if ((5 * M_PI / 8 < theta && theta < 7 * M_PI / 8)
               || (-3 * M_PI / 8 < theta && theta < -M_PI / 8)) {
        grad1 = std::get<0>(grads(0, 0));
        grad2 = std::get<0>(grads(2, 2));
        if (!(grad > grad1 && grad > grad2)) {
            return std::make_tuple(0, 0, 0);
        }
    } else {
        std::cerr << "Achtung! MaxGradMapOp operator() went 3d."
                  << std::endl;
        return std::make_tuple(-1, -1, -1);
    }
    
    return grads(1, 1);
}

template<typename T>
class ArithmeticFilterOp
{
    T _radius;
    T _radius_i;
    T _radius_j;
public:
    ArithmeticFilterOp()
        : _radius(0), _radius_i(0), _radius_j(0), radius(0) {};

    virtual ~ArithmeticFilterOp() {};

    // This is a sad requirement (design limitation).
    T radius;

    T getRadius() const
    {
        return _radius;
    }

    T getRadiusi() const {return _radius_i;}
    T getRadiusj() const {return _radius_j;}
};

template<typename T>
class DoubleOp : public ArithmeticFilterOp<T>
{
 public:
    DoubleOp() : ArithmeticFilterOp<T>() {};

    virtual ~DoubleOp() {};

    virtual std::tuple<double, double, double>
    operator() (const Matrix<std::tuple<T, T, T>> &m) const
    {
        T r, g, b;
        std::tie(r, g, b) = m(0, 0);
        return std::make_tuple(double(r), double(g), double(b));
    }
};

template<typename T>
class UintOp : public ArithmeticFilterOp<T>
{
public:
    UintOp() : ArithmeticFilterOp<T>() {};
    
    virtual ~UintOp() {};

    virtual std::tuple<uint, uint, uint>
    operator() (const Matrix<std::tuple<T, T, T>> &m) const
    {
        T r, g, b;
        std::tie(r, g, b) = m(0, 0);
        if (r < 0) {
            r = 0;
        } else if (r > 255) {
            r = 255;
        }
        if (g < 0) {
            g = 0;
        } else if (g > 255) {
            g = 255;
        }
        if (b < 0) {
            b = 0;
        } else if (b > 255) {
            b = 255;
        }
        
        return std::make_tuple(uint(r), uint(g), uint(b));
    }
};

template<typename T>
class UnaryArithmeticOp : public ArithmeticFilterOp<T>
{
 public:
    UnaryArithmeticOp() : ArithmeticFilterOp<T>() {};

    virtual ~UnaryArithmeticOp() {};
    
    virtual std::tuple<T, T, T>
        operator() (const Matrix<std::tuple<T, T, T>> &) const = 0;
};

template <typename T>
class PowerFilterOp : public UnaryArithmeticOp<T>
{
    double _power;
 public:
    PowerFilterOp(double power)
        : UnaryArithmeticOp<T>(), _power(power) {};
    
    virtual ~PowerFilterOp() {};

    virtual std::tuple<T, T, T>
    operator() (const Matrix<std::tuple<T, T, T>> &) const override;
};

template<typename T>
std::tuple<T, T, T>
PowerFilterOp<T>::operator() (const Matrix<std::tuple<T, T, T>> &m) const
{
    using std::pow;
    T r, g, b; double p = _power;
    auto half_size = (2 * this->radius + 1) / 2;
    std::tie(r, g, b) = m(half_size, half_size);
    return std::make_tuple(pow(r, p), pow(g, p), pow(b, p));
}

template<typename T>
class BinaryArithmeticOp : public ArithmeticFilterOp<T>
{
 public:
    BinaryArithmeticOp() : ArithmeticFilterOp<T>() {};

    virtual ~BinaryArithmeticOp() {};

    virtual std::tuple<T, T, T>
    operator() (const Matrix<std::tuple<T, T, T>> &,
                const Matrix<std::tuple<T, T, T>> &) const = 0;
};

template<typename T>
class SumFilter : public BinaryArithmeticOp<T>
{
 public:
    SumFilter() : BinaryArithmeticOp<T>() {};

    virtual ~SumFilter() {};

    std::tuple<T, T, T>
    operator() (const Matrix<std::tuple<T, T, T>> &,
                const Matrix<std::tuple<T, T, T>> &) const override;

};

template<typename T>
std::tuple<T, T, T>
SumFilter<T>::operator() (const Matrix<std::tuple<T, T, T>> &lhs,
                          const Matrix<std::tuple<T, T, T>> &rhs) const
{
    T lr, lg, lb;
    T rr, rg, rb;
    auto half_size = this->getRadius();
    std::tie(lr, lg, lb) = lhs(half_size, half_size);
    std::tie(rr, rg, rb) = rhs(half_size, half_size);
    return std::make_tuple(rr + lr, lg + rg, lb + rb);
}

template<typename T>
class SubFilter : public BinaryArithmeticOp<T>
{
 public:
    SubFilter() {};
    
    virtual ~SubFilter() {};

    virtual std::tuple<T, T, T>
    operator() (const Matrix<std::tuple<T, T, T>> &,
                const Matrix<std::tuple<T, T, T>> &) const override;
};

template<typename T>
std::tuple<T, T, T>
SubFilter<T>::operator() (const Matrix<std::tuple<T, T, T>> &lhs,
            const Matrix<std::tuple<T, T, T>> &rhs) const
{
    T lr, lg, lb;
    T rr, rg, rb;
    auto half_size = this->getRadius();
    std::tie(lr, lg, lb) = lhs(half_size, half_size);
    std::tie(rr, rg, rb) = rhs(half_size, half_size);
    return std::make_tuple(rr - lr, lg - rg, lb - rb);
}

template<typename T>
class Atan2Filter : public BinaryArithmeticOp<T>
{
 public:
    Atan2Filter() {};

    ~Atan2Filter() {};

    std::tuple<T, T, T>
    operator() (const Matrix<std::tuple<T, T, T>> &,
                const Matrix<std::tuple<T, T, T>> &) const override;
};

template<typename T>
std::tuple<T, T, T>
Atan2Filter<T>::operator()
    (const Matrix<std::tuple<T, T, T>> &lhs,
     const Matrix<std::tuple<T, T, T>> &rhs) const
{
    using std::atan2;
    auto rad = this->getRadius() / 2;
    T lr, lg, lb, rr, rg, rb; // soo ugly.

    std::tie(lr, lg, lb) = lhs(rad, rad);
    std::tie(rr, rg, rb) = rhs(rad, rad);

    return std::make_tuple(atan2(lr, rr), atan2(lg, rg), atan2(lb, rb));
}
