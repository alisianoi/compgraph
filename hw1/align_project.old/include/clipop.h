#pragma once

#include <vector>

#include "io.h"
#include "canny.h"

Image
clip_border(const Image &src_image,
            double threshold1, double threshold2);

class ClipOp final
{
    Image _map0255;
    uint _row_peek;
    uint _col_peek;
    std::array<uint, 2> _rowclip;
    std::array<uint, 2> _colclip;
public:
    ClipOp(const Image &map0255);

    uint find_one_of_two_borders(std::vector<uint> &,
                                 const std::string &);

    // well, don't really get high...
    auto getHigh() -> decltype(_rowclip[0]) {return _rowclip[0];}
    auto getLow() -> decltype(_rowclip[1]) {return _rowclip[1];}

    auto getLeft() -> decltype(_colclip[0]) {return _colclip[0];}
    auto getRight() -> decltype(_colclip[1]) {return _colclip[1];}
};
