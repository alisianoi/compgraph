#pragma once

#include <algorithm>
#include <vector>

#include "io.h"
#include "arithmetic.hpp"

Image
autocontrast(Image src_image, double fraction);
