#pragma once

#include <vector>

#include "io.h"
#include "filterop.h"
#include "arithmetic.hpp"
#include "custom.h"

Image
unsharp(Image);

class UnsharpFilterOp : public FilterOp
{
    std::vector<std::vector<double>> _kernel;
 public:
    UnsharpFilterOp();

    virtual std::tuple<double, double, double>
        operator() (const SDMatrix &m) const;
};
