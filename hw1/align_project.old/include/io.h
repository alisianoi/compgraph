#pragma once

#include "matrix.h"
#include "EasyBMP.h"

#include <tuple>

typedef Matrix<std::tuple<uint, uint, uint>> Image;

// signed double matrix for inner computations
typedef Matrix<std::tuple<double, double, double>> SDMatrix;

// matrix for maps might be short...
typedef Matrix<std::tuple<short, short, short>> Map;

Image load_image(const char*);
void save_image(const Image&, const char*);
