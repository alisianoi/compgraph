#pragma once

#include <iostream>

#include "filterop.h" // for FilterOp
#include "arithmetic.hpp" // for transforms

// apply gaussian filter to src_image
// generate gaussian kernel of size (2 * radius + 1) X (2 * radius + 1)
// using sigma as sample variance
Image
gaussian(Image src_image, double sigma, int radius);

Image
gaussian_separable(Image src_image, double sigma, int radius);

SDMatrix
gaussian(SDMatrix src_image, double sigma, int radius);

SDMatrix
gaussian_separable(SDMatrix src_image, double sigma, int radius);

class GaussianFilterOp : public FilterOp
{
    double _sigma;
 protected:
    long double _cumsum;
 public:
    GaussianFilterOp(double s, int r);

    virtual ~GaussianFilterOp() = 0;

    // a few inline getters
    virtual auto getSigma() const -> decltype(_sigma) final;
    virtual auto getCumSum() const -> decltype(_cumsum) final;

    virtual std::tuple<double, double, double>
        operator() (const SDMatrix &m) const override = 0;
};

class GaussianFilterOpNaive final : public GaussianFilterOp
{
    Matrix<double> _kernel;
 public:
    GaussianFilterOpNaive(double s, int r);
    GaussianFilterOpNaive(Matrix<double> kernel);

    virtual ~GaussianFilterOpNaive() override;

    virtual double gaussiana(double x, double y) const;
    virtual void show_kernel(std::ostream &out) const final;
    
    virtual std::tuple<double, double, double>
        operator() (const SDMatrix &m) const override;
};

class GaussianFilterOpSeparate final : public GaussianFilterOp
{
    Matrix<double> _vec;
    enum states {HORIZONTAL, VERTICAL, SUM};
    mutable enum states state;
 public:
    GaussianFilterOpSeparate(double s, int r);

    virtual ~GaussianFilterOpSeparate() override;

    virtual double gaussiana(double x) const;
    virtual void show_kernel(std::ostream &out) const final;

    virtual std::tuple<double, double, double>
        operator() (const SDMatrix &m) const override;
};
