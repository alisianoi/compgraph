#pragma once
#include "filterop.h" // for FilterOp
#include "arithmetic.hpp"

Image
sobel_x(Image src_image);

Image
sobel_y(Image src_image);

SDMatrix
sobel_x(SDMatrix src_image);

SDMatrix
sobel_y(SDMatrix src_image);

class SobelFilterOp : public FilterOp
{
 protected:
    Matrix<double> _kernel;
 public:
    SobelFilterOp(int r);
    SobelFilterOp(int r, Matrix<double> &kernel);
    
    virtual ~SobelFilterOp();
    
    virtual std::tuple<double, double, double>
        operator() (const SDMatrix &m) const final;
};

class SobelFilterOpX final : public SobelFilterOp
{
 public:
    SobelFilterOpX(int r);

    virtual ~SobelFilterOpX() override;
};

class SobelFilterOpY final : public SobelFilterOp
{
 public:
    SobelFilterOpY(int r);

    virtual ~SobelFilterOpY() override;
};
