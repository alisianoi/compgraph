#pragma once

#include "io.h"
#include "filterop.h"
#include "arithmetic.hpp"

Image
custom(Image, Matrix<double>);

class CustomFilterOp : public FilterOp
{
    Matrix<double> _kernel;
 public:
    CustomFilterOp(std::size_t i, std::size_t j, Matrix<double> kernel);

    std::tuple<double, double, double>
        operator() (const SDMatrix &m) const;
};
