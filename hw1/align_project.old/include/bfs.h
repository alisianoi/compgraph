#pragma once

#include <set>
#include <array>
#include <queue>
#include <climits>

#include "io.h"

class BFS final
{
    Image _map;
 public:
    BFS(const Image &);

    Image operator() ();
 private:
    void start_bfs(int i, int j);
};
