#pragma once

#include "io.h"

long double
mean_squared_estimate(const Image &, const Image &);

unsigned long long
cross_correlation(const Image &, const Image &);
