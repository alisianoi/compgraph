#pragma once

#include "io.h" // for Image
#include "sobel.h" // for Sobel
#include "gaussian.h" // for gaussian and gaussian_separable
#include "arithmetic.hpp"
#include "thresholdop.h" // for ThresholdOp
#include "bfs.h" // for breadth first search
#include "clipop.h" // for final 5% clippingo

Image
canny(Image src_image, double threshold1, double threshold2);
