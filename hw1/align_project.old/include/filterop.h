#pragma once
#include "io.h"

class FilterOp
{
    unsigned int _radius;
    unsigned int _radius_i;
    unsigned int _radius_j;
 public:
    FilterOp(unsigned int r);
    FilterOp(std::size_t i, std::size_t j);

    virtual ~FilterOp();
    // the following `int radius` is an implementation requirement
    // I deeply disagree with but have to adhere to.
    unsigned int radius;

    // a few inline getters
    auto getRadius() const -> decltype(_radius);
    auto getRadiusi() const -> decltype(_radius_i);
    auto getRadiusj() const -> decltype(_radius_j);

    virtual std::tuple<double, double, double>
        operator() (const SDMatrix &m) const = 0;
};
