#pragma once

#include <string>
#include <utility>

#include "io.h"
#include "canny.h"
#include "resize.h"
#include "metrics.h"
#include "grayworld.h"
#include "unsharpen.h"
#include "autocontrast.h"

std::array<Image, 3>
slice3(Image);

Image
align(Image, std::string);

Image
align(Image, std::string, double);

unsigned int
get_longest_side(const Image &, const Image &, const Image &);

std::pair<int, int>
alignment(const Image &, const Image &, const int);
