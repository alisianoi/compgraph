#pragma once

#include "io.h" // for Image and SDMatrix
#include "arithmetic.hpp" // for DoubleOp and UintOp

Image
gray_world(const Image &src_image);
