#include "unsharpen.h"

Image
unsharp(Image src_image)
{
    Matrix<double> _kernel = {
        {-1.0/6,  -2.0/3,   -1.0/6},
        {-2.0/3, 4 + 1.0/3, -2.0/3},
        {-1.0/6,  -2.0/3,   -1.0/6}
    };

    return custom(src_image, _kernel);
}
