#include "thresholdop.h"

ThresholdOp::ThresholdOp(const SDMatrix &map, double thresh1,
                         double thresh2)
    : _map(map), _thresh1(thresh1), _thresh2(thresh2)
{}

Image
ThresholdOp::operator() ()
{
    auto rows = _map.getRows();
    auto cols = _map.getCols();
    Image result(rows, cols);

    double r, g, b;
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            std::tie(r, g, b) = _map(i, j);
            if (r > _thresh2) {
                result(i, j) = std::make_tuple(255, 255, 255);
            } else if (r < _thresh1) {
                result(i, j) = std::make_tuple(0, 0, 0);
            } else {
                result(i, j) = std::make_tuple(2, 2, 2);
            }
        }
    }

    return result;
}
