#include "metrics.h"

long double
mean_squared_estimate(const Image &image1, const Image &image2)
{
    using std::get;
    using std::min;
    using std::pow;
    unsigned int rows = min(image1.getRows(), image2.getRows());
    unsigned int cols = min(image1.getCols(), image2.getCols());

    long double result = 0;
    for (unsigned int i = 0; i < rows; ++i) {
        for (unsigned int j = 0; j < cols; ++j) {
            result += pow(get<0>(image1(i, j)) - get<0>(image2(i, j)), 2);
        }
    }

    return result / rows / cols;
}


unsigned long long
cross_correlation(const Image &image1, const Image &image2)
{
    using std::get;
    using std::min;
    unsigned int rows = min(image1.getRows(), image2.getRows());
    unsigned int cols = min(image1.getCols(), image2.getCols());
    
    unsigned long long result = 0;
    for (unsigned int i = 0; i < rows; ++i) {
        for (unsigned int j = 0; j < cols; ++j) {
            result += get<0>(image1(i, j)) * get<0>(image2(i, j));
        }
    }

    return result;
}
