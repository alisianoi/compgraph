#include "custom.h"

Image
custom(Image src_image, Matrix<double> kernel)
{  
    DoubleOp<uint> uint2double = DoubleOp<uint>();
    SDMatrix result = src_image.unary_map(uint2double);
 
    auto radiusi = kernel.getRows() / 2;
    auto radiusj = kernel.getCols() / 2;
    CustomFilterOp customf(radiusi, radiusj, kernel);
    result = result.unary_map(customf);

    UintOp<double> double2uint = UintOp<double>();
    return result.unary_map(double2uint);
}

CustomFilterOp::CustomFilterOp(std::size_t i, std::size_t j, Matrix<double> kernel)
    : FilterOp(i, j), _kernel(kernel)
{}

std::tuple<double, double, double>
CustomFilterOp::operator() (const SDMatrix &m) const
{
    // stay dumb: apply kernel to image;

    double r, g, b;
    std::size_t rows = 2 * getRadiusi() + 1;
    std::size_t cols = 2 * getRadiusj() + 1;

    double r_result = 0, g_result = 0, b_result = 0;
    for (unsigned int i = 0; i < rows; ++i) {
        for (unsigned int j = 0; j < cols; ++j) {
            std::tie(r, g, b) = m(i, j);
            r_result += r * _kernel(i, j);
            g_result += g * _kernel(i, j);
            b_result += b * _kernel(i, j);
        }
    }

    return std::make_tuple(r_result, g_result,  b_result);
}
