#include "resize.h"

Image
resize(Image src_image, double scale)
{
    // const double eps = 1e-4;
    // if (scale < eps) {
    //     std::string msg = "scale is way too small";
    //     throw msg;
    // }

    auto rows = src_image.getRows();
    auto cols = src_image.getCols();

    std::cerr << "rows x cols: " << rows << " " << cols << " at " << scale << std::endl;

    unsigned int new_rows = rows * scale;
    unsigned int new_cols = cols * scale;
    
    // implement as in wikipedia. Ignore the fact that we are inspecting
    // the immediate vicinity: pretend as if we are inspecting some
    // vicinity...
    using std::get;
    using std::make_tuple;
    Image result(new_rows, new_cols);
    for (unsigned int i = 0; i < new_rows; ++i) {
        const unsigned int k = i / scale;
        for (unsigned int j = 0; j < new_cols; ++j) {
            const unsigned int  l = j / scale;
            
            if (k - 1 == UINT_MAX || k + 1 >= rows
                || l - 1 == UINT_MAX || l + 1 >= cols) {
                // this is a border line, just copy.
                result(i, j) = src_image(k, l);
                continue;
            }

            long double y1 = k - 1;
            long double y2 = k + 1;

            long double x1 = l - 1;
            long double x2 = l + 1;
            
            long double k1 = (x2 - l) / (x2 - x1);
            long double k2 = (l - x1) / (x2 - x1);

            auto Q11 = src_image(y1, x1);
            auto Q21 = src_image(y1, x2);

            const auto R1r = k1 * get<0>(Q11) + k2 * get<0>(Q21);
            const auto R1g = k1 * get<1>(Q11) + k2 * get<1>(Q21);
            const auto R1b = k1 * get<2>(Q11) + k2 * get<2>(Q21);

            auto Q12 = src_image(y2, x1);
            auto Q22 = src_image(y2, x2);

            const auto R2r = k1 * get<0>(Q12) + k2 * get<0>(Q22);
            const auto R2g = k1 * get<1>(Q12) + k2 * get<1>(Q22);
            const auto R2b = k1 * get<2>(Q12) + k2 * get<2>(Q22);

            const auto l1 = (y2 - k) / (y2 - y1);
            const auto l2 = (k - y1) / (y2 - y1);

            result(i, j) =
                make_tuple(l1 * R1r + l2 * R2r,
                           l1 * R1g + l2 * R2g,
                           l1 * R1b + l2 * R2b);
        }
    }
    
    return result;
}

