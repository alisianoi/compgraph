#include "sobel.h"

Image
sobel_x(Image src_image)
{
    const DoubleOp<uint> uint2double = DoubleOp<uint>();
    SDMatrix result = src_image.unary_map(uint2double);

    const SobelFilterOpX sobelx = SobelFilterOpX(1);
    result = result.unary_map(sobelx);

    const UintOp<double> double2uint = UintOp<double>();
    return result.unary_map(double2uint);
}

Image
sobel_y(Image src_image)
{

    const DoubleOp<uint> uint2double = DoubleOp<uint>();
    SDMatrix result = src_image.unary_map(uint2double);
    
    const SobelFilterOpY sobely = SobelFilterOpY(1);
    result = result.unary_map(sobely);

    const UintOp<double> double2uint = UintOp<double>();
    return result.unary_map(double2uint);
}

SDMatrix
sobel_x(SDMatrix src_image)
{
    SobelFilterOpX sfx(1);
    return src_image.unary_map(sfx);
}

SDMatrix
sobel_y(SDMatrix src_image)
{
    SobelFilterOpY sfy(1);
    return src_image.unary_map(sfy);
}

SobelFilterOp::SobelFilterOp(int r)
    : FilterOp(r), _kernel(r, r)
{}

SobelFilterOp::SobelFilterOp(int r, Matrix<double> &kernel)
    : FilterOp::FilterOp(r), _kernel(kernel)
{}

SobelFilterOp::~SobelFilterOp()
{}

std::tuple<double, double, double>
SobelFilterOp::operator() (const SDMatrix &m) const
{
    double r, g, b;
    long double r_sum = 0, g_sum = 0, b_sum = 0;

    auto size = 2 * getRadius() + 1;
    for (unsigned int i = 0; i < size; ++i) {
        for (unsigned int j = 0; j < size; ++j) {
            std::tie(r, g, b) = m(i, j);
            r_sum += r * _kernel(i, j);
            g_sum += g * _kernel(i, j);
            b_sum += b * _kernel(i, j);
        }
    }
    
    return std::make_tuple(r_sum, g_sum, b_sum);
}

SobelFilterOpX::SobelFilterOpX(int r)
    : SobelFilterOp::SobelFilterOp(r)
{
    _kernel = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
}

SobelFilterOpX::~SobelFilterOpX()
{}

SobelFilterOpY::SobelFilterOpY(int r)
    : SobelFilterOp::SobelFilterOp(r)
{
    _kernel = {{1, 2, 1}, {0, 0, 0}, {-1, -2, -1}};
}

SobelFilterOpY::~SobelFilterOpY()
{}
