#include <iostream>
#include "matrix.h"
#include "io.h"

using std::cout;
using std::endl;

using std::tuple;
using std::get;
using std::tie;
using std::make_tuple;

// Matrix usage example
// Also see: matrix.h, matrix.hpp for comments on how filtering works

class BoxFilterOp
{
public:
    tuple<uint, uint, uint> operator () (const Image &m) const
    {
        uint size = 2 * radius + 1;
        uint r, g, b, sum_r = 0, sum_g = 0, sum_b = 0;
        for (uint i = 0; i < size; ++i) {
            for (uint j = 0; j < size; ++j) {
                // Tie is useful for taking elements from tuple
                tie(r, g, b) = m(i, j);
                sum_r += r;
                sum_g += g;
                sum_b += b;
            }
        }
        auto norm = size * size;
        sum_r /= norm;
        sum_g /= norm;
        sum_b /= norm;
        return make_tuple(0, 0, 0);
    }
    // Radius of neighbourhood, which is passed to that operator
    static const unsigned int radius = 1;
    static const unsigned int _radius_i = 1;
    static const unsigned int _radius_j = 1;

    auto getRadiusi() const -> decltype(_radius_i) {return _radius_i;}
    auto getRadiusj() const -> decltype(_radius_j) {return _radius_j;}
};

int main(int argc, char **argv)
{
    // Image = Matrix<tuple<uint, uint, uint>>
    // tuple is from c++ 11 standard
    try {
        Image img = load_image(argv[1]);
        Image img2 = img.unary_map(BoxFilterOp());
        save_image(img2, argv[2]);
    } catch (std::string message) {
        std::cerr << message << std::endl;
    }
}
