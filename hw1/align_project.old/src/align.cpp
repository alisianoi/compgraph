#include "align.h"

unsigned int
get_longest_side(const Image &im1, const Image &im2, const Image &im3)
{
    using std::max;
    auto res1 = max(im1.getCols(), im1.getRows());
    auto res2 = max(im2.getCols(), im2.getRows());
    auto res3 = max(im3.getCols(), im3.getRows());

    auto res = max(res1, res2);
    return max(res, res3);
}

Image
align(Image src_image, std::string postprocessing)
{
    std::array<Image, 3> slices = slice3(src_image);

    // let's take some up the sleeve values...
    auto thresh1 = 200;
    auto thresh2 = 300;
    std::array<Image, 3> clipped;
    for (auto i = 0; i < 3; ++i) {
        clipped[i] = clip_border(slices[i], thresh1, thresh2);
    }

    // let's build an image pyramid
    const unsigned int pixels = 400;
    const unsigned int range = 15;
    const double step = 0.5;

    auto longest_side =
        get_longest_side(clipped[0], clipped[1], clipped[2]);

    unsigned int level = 1;
    std::vector<Image> pyramid = {
        clipped[0], clipped[1], clipped[2]
    };

    while (longest_side > pixels) {
        for (auto i = 0; i < 3; ++i) {
            pyramid.push_back(resize(pyramid[3 * level - 3], step));
        }

        longest_side = get_longest_side(pyramid[3 * (level + 1) - 1],
                                        pyramid[3 * (level + 1) - 2],
                                        pyramid[3 * (level + 1) - 3]);
        ++level;
    }

    using std::get;
    using std::make_pair;
    std::pair<int, int> coords1;
    std::pair<int, int> coords2;
    std::pair<int, int> avg_coords;

    for (auto i = level; i != 0; --i) {
        auto x = range * pow(0.5, (level - i));
        coords1 =
            alignment(pyramid[3 * i - 1], pyramid[3 * i - 2], x);
        coords2 = 
            alignment(pyramid[3 * i - 3], pyramid[3 * i - 2], x);
        avg_coords = make_pair((get<0>(coords1) + get<0>(coords2)) / 2,
                               (get<1>(coords1) + get<1>(coords2)) / 2);
        if (i - 1 != 0) {
            auto rows1 = pyramid[3 * (i - 1) - 3].getRows();
            auto cols1 = pyramid[3 * (i - 1) - 3].getCols();
            auto rows2 = pyramid[3 * (i - 1) - 2].getRows();
            auto cols2 = pyramid[3 * (i - 1) - 2].getCols();
            auto rows3 = pyramid[3 * (i - 1) - 1].getRows();
            auto cols3 = pyramid[3 * (i - 1) - 1].getCols();

            auto rel1 = get<0>(coords1);
            auto rel2 = get<1>(coords1);
            auto pr13 = rel1 > 0 ? 0 : -rel1 / step;
            auto pc13 = rel2 > 0 ? 0 : -rel2 / step;
            auto pr2 = rel1 > 0 ? rel1 / step : 0;
            auto pc2 = rel2 > 0 ? rel2 / step : 0;

            pyramid[3 * (i - 1) - 1] =
                pyramid[3 * (i - 1) - 1].submatrix(pr13, pc13, rows3 - pr13, cols3 - pc13);
            pyramid[3 * (i - 1) - 3] =
                pyramid[3 * (i - 1) - 3].submatrix(pr13, pc13, rows1 - pr13, cols1 - pc13);

            
            pyramid[3 * (i - 1) - 2] =
                pyramid[3 * (i - 1) - 2].submatrix(pr2, pc2, rows2 - pr2, cols2 - pc2);
        }
    }

    auto rows1 = pyramid[0].getRows();
    auto cols1 = pyramid[0].getCols();
    auto rows2 = pyramid[1].getRows();
    auto cols2 = pyramid[1].getCols();
    auto rows3 = pyramid[2].getRows();
    auto cols3 = pyramid[2].getCols();
    
    auto rel1 = get<0>(avg_coords);
    auto rel2 = get<1>(avg_coords);
    auto pr13 = rel1 > 0 ? 0 : -rel1 / step;
    auto pc13 = rel2 > 0 ? 0 : -rel2 / step;
    auto pr2 = rel1 > 0 ? rel1 / step : 0;
    auto pc2 = rel2 > 0 ? rel2 / step : 0;
   
    pyramid[2] =
        pyramid[2].submatrix(pr13, pc13, rows3 - pr13, cols3 - pc13);
    pyramid[0] =
        pyramid[0].submatrix(pr13, pc13, rows1 - pr13, cols1 - pc13);
    pyramid[1] =
        pyramid[1].submatrix(pr2, pc2, rows2 - pr2, cols2 - pc2);
    
    auto final_rows = pyramid[0].getRows();
    auto final_cols = pyramid[0].getCols();
    for (auto i = 1; i < 3; ++i) {
        if (pyramid[i].getRows() < final_rows) {
            final_rows = pyramid[i].getRows();
        }
        if (pyramid[i].getCols() < final_cols) {
            final_cols = pyramid[i].getCols();
        }
    }
    
    unsigned int r, g, b;
    using std::make_tuple;
    Image result(final_rows, final_cols);
    for (unsigned int i = 0; i < final_rows; ++i) {
        for (unsigned int j = 0; j < final_cols; ++j) {
            std::tie(b, b, b) = pyramid[0](i, j);
            std::tie(g, g, g) = pyramid[1](i, j);
            std::tie(r, r, r) = pyramid[2](i, j);
            result(i, j) = make_tuple(r, g, b);
        }
    }

    if (postprocessing == "--gray-world") {
        result = gray_world(result);
    } else if (postprocessing == "--unsharp") {
        result = unsharp(result);
    }

    return result;
}

Image
align(Image src_image, std::string postprocessing, double fraction)
{
    Image result = align(src_image, postprocessing);
    return autocontrast(result, fraction);
}

// returns relative coordinates
// ...|...
// ...|...
// ------->
// ...|...
// ...|...
//    V
std::pair<int, int>
alignment(const Image &im1, const Image &im2, const int vicinity)
{
    using std::min;
    using std::abs;
    using std::make_tuple;
    auto cols = min(im1.getCols(), im2.getCols());
    auto rows = min(im1.getRows(), im2.getRows());

    std::vector<std::tuple<long double, int, int>>
        results((2 * vicinity + 1) * (2 * vicinity + 1));

    for (auto i = -vicinity; i < vicinity; ++i) {
        for (auto j = -vicinity; j < vicinity; ++j) {
            auto pr1 = i > 0 ? 0 : -i;
            auto pc1 = j > 0 ? 0 : -j;
            auto pr2 = i > 0 ? i : 0;
            auto pc2 = j > 0 ? j : 0;

            auto I1 =
                im1.submatrix(pr1, pc1, rows - abs(i), cols - abs(j));
            auto I2 = 
                im2.submatrix(pr2, pc2, rows - abs(i), cols - abs(j));
            results[(i + vicinity) * (2 * vicinity + 1) + j + vicinity] =
                make_tuple(mean_squared_estimate(I1, I2), i, j);
        }
    }

    using std::get;
    using std::make_pair;
    sort(results.begin(), results.end());
    return make_pair(get<1>(results[0]), get<2>(results[0]));
}

std::array<Image, 3>
slice3(Image src_image)
{
    auto rows3 = src_image.getRows() / 3;
    auto lastrow = src_image.getRows() - 2 * rows3;
    auto cols = src_image.getCols();
    std::array<Image, 3> slices = {
        src_image.submatrix(0, 0, rows3, cols).deep_copy(),
        src_image.submatrix(rows3, 0, rows3, cols).deep_copy(),
        src_image.submatrix(2 * rows3, 0, lastrow, cols).deep_copy()
    };
    return slices;
}
