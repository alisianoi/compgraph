#include "clipop.h"

Image
clip_border(const Image &src_image,
            double threshold1, double threshold2)
{
    Image BorderMap = canny(src_image, threshold1, threshold2);

    ClipOp clipper(BorderMap);

    return src_image.submatrix(clipper.getHigh(), clipper.getLeft(),
                               clipper.getLow() - clipper.getHigh(),
                               clipper.getRight() - clipper.getLeft());
}

ClipOp::ClipOp(const Image &map0255)
    : _map0255(map0255), _row_peek(0.05 * map0255.getRows()),
      _col_peek(0.05 * map0255.getCols()), _rowclip(), _colclip()
{
    using std::get;

    uint rows = _map0255.getRows();
    uint cols = _map0255.getCols();

    std::vector<uint> row_sums(_row_peek);
    
    for (uint i = 0; i < _row_peek; ++i) {
        row_sums[i] = 0;
    }
    
    for (uint i = 0; i < _row_peek; ++i) {
        for (uint j = 0; j < cols; ++j) {
            row_sums[i] += get<0>(_map0255(i, j));
        }
    }

    _rowclip[0] = find_one_of_two_borders(row_sums, "max");

    for (uint i = 0; i < _row_peek; ++i) {
        row_sums[i] = 0;
    }
    
    for (uint i = rows - _row_peek, k = 0; i < rows; ++i, ++k) {
        for (uint j = 0; j < cols; ++j) {
            row_sums[k] += get<0>(_map0255(i, j));
        }
    }

    _rowclip[1] =
        rows - _row_peek + find_one_of_two_borders(row_sums, "min");

    // now columns

    std::vector<uint> col_sums(_col_peek);
    
    for (uint i = 0; i < _col_peek; ++i) {
        col_sums[i] = 0;
    }

    for (uint j = 0; j < _col_peek; ++j) {
        for (uint i = 0; i < rows; ++i) {
            col_sums[j] += get<0>(_map0255(i, j));
        }
    }

    _colclip[0] = find_one_of_two_borders(col_sums, "max");

    for (uint i = 0; i < _col_peek; ++i) {
        col_sums[i] = 0;
    }

    for (uint j = cols - _col_peek, k = 0; j < cols; ++j, ++k) {
        for (uint i = 0; i < rows; ++i) {
            col_sums[k] += get<0>(_map0255(i, j));
        }
    }

    _colclip[1] =
        cols - _col_peek + find_one_of_two_borders(col_sums, "min");
}

uint
ClipOp::find_one_of_two_borders(std::vector<uint> &sums,
                                const std::string &which)
{
    std::array<uint, 2> clip;

    auto size = sums.size();
    for (auto i = 0; i < 2; ++i) {
        clip[i] = 0;
        for (uint j = 0; j < size; ++j) {
            if (sums[clip[i]] < sums[j]) {
                clip[i] = j;
                if (j > 0) {
                    sums[j - 1] = 0;
                }
                if (j < size - 1) {
                    sums[j + 1] = 0;
                }
                sums[j] = 0;
            }
        }
    }
    
    if (which == "max") {
        return std::max(clip[0], clip[1]);
    } else {
        return std::min(clip[0], clip[1]);
    }
}
