#include "bfs.h"

BFS::BFS(const Image &map)
    : _map(map.deep_copy())
{}

Image
BFS::operator() ()
{
    auto rows = _map.getRows();
    auto cols = _map.getCols();
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            if (std::get<0>(_map(i, j)) == 2) {
                start_bfs(i, j);
            }
        }
    }

    return _map;
}

void
BFS::start_bfs(int i, int j)
{
    using std::get;
    using std::set;
    using std::array;
    using std::queue;
    using std::make_pair;
    using std::make_tuple;

    int rows = _map.getRows();
    int cols = _map.getCols();

    array<int, 2> root = {i, j};
    set<array<int, 2>> Visited;
    queue<array<int, 2>> Queued;

    Queued.push(root);
    Visited.insert(root);

    while (not Queued.empty()) {
        array<int, 2> next = Queued.back(); Queued.pop();
        if (get<0>(_map(next[0], next[1])) == 255) {
            for (auto coord : Visited) {
                _map(coord[0], coord[1]) = make_tuple(255, 255, 255);
            }
            return;
        }
        
        int x = next[0], y = next[1];
        for (int k = -1; k < 2; ++k) {
            for (int l = -1; l < 2; ++l) {
                if (!(k == 0 && l == 0) && x + k >= 0 && x + k < rows && y + l >= 0 && y + l < cols
                    && (get<0>(_map(x + k, y + l)) == 255 || get<0>(_map(x + k, y + l)) == 2)) {
                        array<int, 2> temp = {x + k, y + l};
                        if (Visited.find(temp) == Visited.end()) {
                            Queued.push(temp);
                            Visited.insert(temp);
                        }
                }
            }
        }
    }

    for (auto coord :  Visited) {
        _map(coord[0], coord[1]) = std::make_tuple(0, 0, 0);
    }
}
