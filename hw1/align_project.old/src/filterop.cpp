#include "filterop.h"

FilterOp::FilterOp(unsigned int r)
    : _radius(r), _radius_i(r), _radius_j(r), radius(r)
{}

FilterOp::FilterOp(std::size_t i, std::size_t j)
    : _radius(-1), _radius_i(i), _radius_j(j), radius(-1)
{}

FilterOp::~FilterOp()
{}

auto
FilterOp::getRadius() const -> decltype(_radius)
{
    if (_radius_i != _radius_j) {
        throw "trying to getRadius() for a non-square filter";
    }

    return _radius;
}

auto
FilterOp::getRadiusi() const -> decltype(_radius_i)
{
    return _radius_i;
}

auto
FilterOp::getRadiusj() const -> decltype(_radius_j)
{
    return _radius_j;
}
