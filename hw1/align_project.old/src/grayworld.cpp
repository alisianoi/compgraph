#include "grayworld.h"

Image
gray_world(const Image &src_image)
{
    unsigned int rows = src_image.getRows();
    unsigned int cols = src_image.getCols();

    Image dst_image(src_image);

    DoubleOp<uint> uint2double = DoubleOp<uint>();
    SDMatrix image = dst_image.unary_map(uint2double);

    double r, g, b;
    long double Sr = 0, Sg = 0, Sb = 0, S = 0;
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            std::tie(r, g, b) = image(i, j);
            Sr += r;
            Sg += g;
            Sb += b;
        }
    }

    // now average Sr, Sg and Sb out
    unsigned long long total = cols * rows;
    Sr = Sr / total;
    Sg = Sg / total;
    Sb = Sb / total;

    S = (Sr + Sg + Sb) / 3;

    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            std::tie(r, g, b) = image(i, j);
            r = r * S / Sr;
            g = g * S / Sg;
            b = b * S / Sb;
            image(i, j) = std::make_tuple(r, g, b);
        }
    }

    UintOp<double> double2uint = UintOp<double>();
    return image.unary_map(double2uint);
}
