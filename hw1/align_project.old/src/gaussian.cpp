#include "gaussian.h"

Image
gaussian(Image src_image, double sigma, int radius)
{
    DoubleOp<uint> uint2double = DoubleOp<uint>();
    SDMatrix result = src_image.unary_map(uint2double);

    GaussianFilterOpNaive gfn(sigma, radius);
    result = result.unary_map(gfn);

    UintOp<double> double2uint = UintOp<double>();
    return result.unary_map(double2uint);
}

Image
gaussian_separable(Image src_image, double sigma, int radius)
{
    DoubleOp<uint> uint2double = DoubleOp<uint>();
    SDMatrix result = src_image.unary_map(uint2double);

    GaussianFilterOpSeparate gfs(sigma, radius);
    result = result.unary_map(gfs);

    UintOp<double> double2uint = UintOp<double>();
    return result.unary_map(double2uint);
}

SDMatrix
gaussian(SDMatrix src_image, double sigma, int radius)
{
    GaussianFilterOpNaive gf(sigma, radius);
    return src_image.unary_map(gf);
}

SDMatrix
gaussian_separable(SDMatrix src_image, double sigma, int radius)
{
    // this function is ugly because of the following design flaw:
    // images should not let filters modify them via methods:
    // Image::some_map(SomeFilter filter);
    // Instead, filters should create Images based on a source Image:
    // new_image = filter(image);
    // with move construtors/assignment operators 

    // here is my problem: I need a more specific interaction with the
    // Image (aka Matrix) class, which will not be provided by either
    // unary_map, binary_map or any other currently implemented
    // method.  Implementing my own method requires digging the
    // internals of Image, which I am not looking forward
    // to. Moreover, I disagree with Images' current design pattern
    // (modyfies itself with third-party functors) which is why I'll
    // implement a stateful filter (it's ugly) and live with that.

    // should be applied 3 times to get the effect;g
    GaussianFilterOpSeparate gfs(sigma, radius);
    src_image.unary_map(gfs);
    src_image.unary_map(gfs);
    return src_image.unary_map(gfs);

    // Moreover, the extra-3-point task says "for class Matrix
    // implement mirroring, because when any filter is applied to the
    // border, it gets clipped". Wrong! The image should not pad
    // itself, it's the job of the filter. And this padding should not
    // be stored (it gets invalidated after any single filter
    // application), it should get discarded after every filter
    // transform.
}

GaussianFilterOp::GaussianFilterOp(double s, int r)
    : FilterOp::FilterOp(r), _sigma(s), _cumsum(0)
{}

GaussianFilterOp::~GaussianFilterOp()
{}

auto
GaussianFilterOp::getSigma() const -> decltype(_sigma)
{
    return _sigma;
}

auto
GaussianFilterOp::getCumSum() const -> decltype(_cumsum)
{
    return _cumsum;
}

GaussianFilterOpNaive::GaussianFilterOpNaive(double s, int r)
    : GaussianFilterOp::GaussianFilterOp(s, r),
      _kernel(2 * r + 1, 2 * r + 1)
{
    auto size = 2 * radius + 1;
    for (unsigned int i = 0; i < size; ++i) {
        for (unsigned int j = 0; j < size; ++j) {
            _kernel(i, j) = gaussiana(i, j);
            _cumsum += _kernel(i, j);
        }
    }

    // normalize matrix elements now
    for (unsigned int i = 0; i < size; ++i) {
        for (unsigned int j = 0; j < size; ++j) {
            _kernel(i, j) /= _cumsum;
        }
    }
}

GaussianFilterOpNaive::GaussianFilterOpNaive(Matrix<double> kernel)
    : GaussianFilterOp::GaussianFilterOp(-1, kernel.getRows()),
      _kernel(kernel)
{
    if (kernel.getRows() != kernel.getCols()) {
        throw std::string ("source kernel is messed up, abort");
    }
        
    unsigned long long size = 2 * getRadius() + 1;
    for (unsigned long long i = 0; i < size; ++i) {
        for (unsigned long long j = 0; j < size; ++j) {
            _cumsum += _kernel(i, j);
        }
    }

    // we don't know if the source kernel is normalized,
    // let's check that.
    const double eps = pow(10, -5);
    if (1 - eps < _cumsum && _cumsum < 1 + eps) {
        for (unsigned long long i = 0; i < size; ++i) {
            for (unsigned long long j = 0; j < size; ++j) {
                _kernel(i, j) /= _cumsum;
            }
        }
    }

    // I don't know whether it's possible to extract sigma...
    // Forget it.
}

GaussianFilterOpNaive::~GaussianFilterOpNaive()
{}

double
GaussianFilterOpNaive::gaussiana(double x, double y) const
{
    using std::pow;
    using std::abs;

    // think on refactoring this out, height doesn't matter
    auto height = 1;
    // name radius is occupied and I am not using it religiousely 
    auto radius_ = getRadius();
    auto sigma = getSigma();
    auto dist2 =  pow(abs(x - radius_), 2) + pow(abs(y - radius_), 2);
    return height * exp(-dist2 / 2 / pow(sigma, 2));
}

void GaussianFilterOpNaive::show_kernel(std::ostream &out) const
{
    auto size = 2 * radius + 1;
    for (unsigned int i = 0; i < size; ++i) {
        for (unsigned int j = 0; j < size; ++j) {
            out << _kernel(i, j) << " ";
        }
        out << std::endl;
    }
}
    
std::tuple<double, double, double>
GaussianFilterOpNaive::operator() (const SDMatrix &m) const
{
    // here mimic the matrix_example.cpp, even in using radius...
    double r, g, b;
    double size = 2 * radius + 1;
    double r_result = 0, g_result = 0, b_result = 0;
    for (auto i = 0; i < size; ++i) {
        for (auto j = 0; j < size; ++j) {
            std::tie(r, g, b) = m(i, j);
            r_result += r * _kernel(i, j);
            g_result += g * _kernel(i, j);
            b_result += b * _kernel(i, j);
        }
    }
    // no need to normalize --- _kernel is already normalized
    return std::make_tuple(r_result, g_result,  b_result);
}

GaussianFilterOpSeparate::GaussianFilterOpSeparate(double s, int r)
    : GaussianFilterOp::GaussianFilterOp(s, r), _vec(1, 2 * r + 1),
      state(HORIZONTAL)
{
    auto size = 2 * radius + 1;
    for (unsigned int i = 0; i < size; ++i) {
        _vec(0, i) = gaussiana(i);
        _cumsum += _vec(0, i);
    }

    // normalize vector now
    for (unsigned int i = 0; i < size; ++i) {
        _vec(0, i) /= _cumsum;
    }
}

GaussianFilterOpSeparate::~GaussianFilterOpSeparate()
{}

double
GaussianFilterOpSeparate::gaussiana(double x) const
{
    using std::pow;
    using std::abs;

    // think on refactoring this out, height doesn't matter
    auto height = 1;
    // name radius is occupied and I am not using it religiousely 
    auto radius_ = getRadius();
    auto sigma = getSigma();
    auto dist2 = pow(abs(x - radius_), 2);
    return height * exp(-dist2 / 2 / pow(sigma, 2));
}

void
GaussianFilterOpSeparate::show_kernel(std::ostream &out) const
{
    auto size = 2 * getRadius() + 1;
    for (unsigned int i = 0; i < size; ++i) {
        out << _vec(0, i) << " ";
    }
    out << std::endl;
}

std::tuple<double, double, double>
GaussianFilterOpSeparate::operator() (const SDMatrix &m) const
{
    auto half_size = getRadius();
    auto size = 2 * half_size + 1;
    double r_sum = 0, g_sum = 0, b_sum = 0;
    double r, g, b;
    switch(state) {
    case HORIZONTAL:
        for (unsigned int i = 0; i < size; i++) {
            std::tie(r, g, b) = m(half_size, i);
            r_sum += r * _vec(0, i);
            g_sum += g * _vec(0, i);
            b_sum += b * _vec(0, i);
        }
        state = VERTICAL;
        break;
    case VERTICAL:
        for (unsigned int i = 0; i < size; i++) {
            std::tie(r, g, b) = m(i, half_size);
            r_sum += r * _vec(0, i);
            g_sum += g * _vec(0, i);
            b_sum += b * _vec(0, i);
        }
        break;
        state = SUM;
    case SUM:
        for (unsigned int i = 0; i < size; ++i) {
            for (unsigned int j = 0; j < size; ++j) {
                std::tie(r, g, b) = m(i, half_size);
                r_sum += r;
                g_sum += g;
                b_sum += b;
            }
        }
        state = HORIZONTAL;
        break;
    default:
        break;
    }

    return std::make_tuple(r_sum, g_sum, b_sum);
}
