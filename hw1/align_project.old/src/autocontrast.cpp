#include "autocontrast.h"

Image
autocontrast(Image src_image, double fraction)
{
    DoubleOp<uint> uint2double = DoubleOp<uint>();
    SDMatrix image = src_image.unary_map(uint2double);

    auto cols = src_image.getCols();
    auto rows = src_image.getRows();
    unsigned int halfway = cols * rows * fraction / 2;
    unsigned long long int n = cols * rows;

    Matrix<double> mluminance(rows, cols);
    std::vector<double> luminances(n);

    double r, g, b;
    for (unsigned int i = 0; i < rows; ++i) {
        for (unsigned int j = 0; j < cols; ++j) {
            std::tie(r, g, b) = image(i, j);
            luminances[i * cols + j] =
                0.2125 * r + 0.7154 * g + 0.0721 * b;
            mluminance(i, j) = 
                0.2125 * r + 0.7154 * g + 0.0721 * b;
        }
    }

    std::sort(luminances.begin(), luminances.end());
    
    double y_min = luminances[halfway];
    double y_max = luminances[n - halfway];
    double delta = y_max - y_min;

    for (unsigned int i = 0; i < rows; ++i) {
        for (unsigned int j = 0; j < cols; ++j) {
            // make relative coeffs at once
            if (mluminance(i, j) < y_min) {
                mluminance(i, j) = 0; // corresponds to pitch black
            } else if (mluminance(i, j) > y_max) {
                mluminance(i, j) = 1; // corresponds to clear white
            } else {
                mluminance(i, j) =
                    (mluminance(i, j) - y_min) / delta ;
            }

            std::tie(r, g, b) = image(i, j);
            r = r * mluminance(i, j);
            g = g * mluminance(i, j);
            b = b * mluminance(i, j);
            image(i, j) = std::make_tuple(r, g, b);
        }
    }

    UintOp<double> double2uint = UintOp<double>();
    return image.unary_map(double2uint);
}
