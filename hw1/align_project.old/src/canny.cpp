#include "canny.h"

Image
canny(Image src_image, double threshold1, double threshold2)
{
    DoubleOp<uint> uint2double = DoubleOp<uint>();
    UintOp<double> double2uint = UintOp<double>();
    SDMatrix sdm_image = src_image.unary_map(uint2double);

    // taken sigma 1.4 and radius 2 as in the task description
    sdm_image = gaussian_separable(sdm_image, 1.4, 2);

    SDMatrix Ix = sobel_x(sdm_image);
    SDMatrix Iy = sobel_y(sdm_image);

    PowerFilterOp<double> power2 = PowerFilterOp<double>(2);
    SDMatrix Ix2 = Ix.unary_map(power2);
    SDMatrix Iy2 = Iy.unary_map(power2);
    
    SumFilter<double> sumf = SumFilter<double>();
    SDMatrix Ix2Iy2 =
        binary_map<double>(sumf, Ix2, Iy2);
    
    PowerFilterOp<double> power_half = PowerFilterOp<double>(0.5);
    SDMatrix G = Ix2Iy2.unary_map(power_half);

    Atan2Filter<double> atan2f = Atan2Filter<double>();
    SDMatrix theta =  binary_map<double>(atan2f, Ix, Iy);

    MaxGradMapOp<double> build_map = MaxGradMapOp<double>();
    SDMatrix GwoNonMax = binary_map<double>(build_map, G, theta);

    ThresholdOp thresholder =
        ThresholdOp(G, threshold1, threshold2);
    Image GwoNonMaxThreshed = thresholder();

    BFS bfs(GwoNonMaxThreshed);
    Image map0255 = bfs();

    return map0255;
}
