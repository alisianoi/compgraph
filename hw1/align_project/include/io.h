#pragma once

#include "matrix.h"
#include "EasyBMP.h"

#include <tuple>

typedef Matrix<std::tuple<float, float, float>> Image;

Image load_image(const char*);
void save_image(const Image&, const char*);
void save_image(const Matrix<double>&, const char*);
void save_image(const Matrix<uint>&, const char*);
