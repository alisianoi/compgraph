#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <initializer_list>
#include <limits>
#include <cmath>
#include <queue>
#include <set>
#include <array>
#include <climits>
#include <algorithm>

using std::string;
using std::stringstream;
using std::cout;
using std::cerr;
using std::endl;
using std::numeric_limits;

#include "io.h"
#include "matrix.h"

void print_help(const char *argv0)
{
    const char *usage =
R"(where PARAMS are from list:

--align [--gray-world | --unsharp | --autocontrast [<fraction>]]
    align images with one of postprocessing functions

--gaussian <sigma> [<radius>=1]
    gaussian blur of image, 0.1 < sigma < 100, radius = 1, 2, ...

--gaussian-separable <sigma> [<radius>=1]
    same, but gaussian is separable

--sobel-x
    Sobel x derivative of image

--sobel-y
    Sobel y derivative of image

--unsharp
    sharpen image

--gray-world
    gray world color balancing

--autocontrast [<fraction>=0.0]
    autocontrast image. <fraction> of pixels must be croped for robustness

--resize <scale>
    resize image with factor scale. scale is real number > 0

--canny <threshold1> <threshold2>
    apply Canny filter to grayscale image. threshold1 < threshold2,
    both are in 0..360

--custom <kernel_string>
    convolve image with custom kernel, which is given by kernel_string, example:
    kernel_string = '1,2,3;4,5,6;7,8,9' defines kernel of size 3

[<param>=default_val] means that parameter is optional.
)";
    cout << "Usage: " << argv0 << " <input_image_path> <output_image_path> "
         << "PARAMS" << endl;
    cout << usage;
}

template<typename ValueType>
ValueType read_value(string s)
{
    stringstream ss(s);
    ValueType res;
    ss >> res;
    if (ss.fail() or not ss.eof())
        throw string("bad argument: ") + s;
    return res;
}

template<typename ValueT>
void check_number(string val_name, ValueT val, ValueT from,
                  ValueT to=numeric_limits<ValueT>::max())
{
    if (val < from)
        throw val_name + string(" is too small");
    if (val > to)
        throw val_name + string(" is too big");
}

void check_argc(int argc, int from, int to=numeric_limits<int>::max())
{
    if (argc < from)
        throw string("too few arguments for operation");

    if (argc > to)
        throw string("too many arguments for operation");
}

Matrix<double> parse_kernel(string kernel)
{
    // Kernel parsing implementation here
    // no, I don't know how to program


    if (*(kernel.end() - 1) != ';') {
        kernel.push_back(';');
    }    
    
    std::size_t rows = std::count(kernel.begin(), kernel.end(), ';');
    unsigned long long cols = 0;
    for (auto s = kernel.begin(); *s != ';'; ++s) {
        if (*s == ',') {
            ++cols;
        }
    } ++cols;

    if (rows % 2 == 0) {
        std::string msg = "rows are not odd, kernel has no obvious center.";
        throw msg;
    } else if (cols % 2 == 0) {
        std::string msg = "cols are not odd, kernel has no obvious center.";
        throw msg;
    }

    char delim;
    Matrix<double> mkernel(rows, cols);
    std::stringstream skernel(kernel);
    for (unsigned long long i = 0; i < rows; ++i) {
        for (unsigned long long j = 0; j < cols; ++j) {
            skernel >> std::noskipws >> mkernel(i, j) >> delim;
            if (skernel.fail() || skernel.eof()) {
                std::string msg = "Weird kernel, retry when sober.";
                throw msg;
            }
            if (j + 1 == cols and delim != ';') {
                if (i + 1 != rows) {
                    std::string msg = "failed parsing string: semicolon expeceted";
                    throw msg;
                }
            } else if (j + 1 != cols and delim != ',') {
                std::string msg = "failed parsing string: comma expeceted";
                throw msg;
            }
        }
    }

    skernel >> delim;

    if (not skernel.eof()) {
        std::string trash;
        skernel >> trash;
        std::string msg = "trash at end of kernel is not dope";
        throw msg;
    }

    return mkernel;
}

class FilterOp
{
    Matrix<double> _kernel;
public:
    const int _radius_i;
    const int _radius_j;

    FilterOp(Matrix<double> k)
        : _kernel(k), _radius_i(k.n_rows / 2), _radius_j(k.n_cols / 2)
    {}

    std::tuple<float, float, float>
    operator () (const Image &m) const
    {
        unsigned int k_rows = 2 * _radius_i + 1;
        unsigned int k_cols = 2 * _radius_j + 1;
        // if (m.n_rows != k_rows || m.n_cols != k_cols) {
        //     std::string msg =
        //         "filter dimensions and matrix dimensions mismatch";
        //     throw msg;
        // }

        double r, g, b;
        double sum_r = 0, sum_g = 0, sum_b = 0;
        for (uint i = 0; i < k_rows; ++i) {
            for (uint j = 0; j < k_cols; ++j) {
                std::tie(r, g, b) = m(i, j);
                sum_r += r * _kernel(i, j);
                sum_g += g * _kernel(i, j);
                sum_b += b * _kernel(i, j);
            }
        }

        return std::make_tuple(sum_r, sum_g, sum_b);
    }
};

// it's bloody definitions time!

typedef std::array<Matrix<double>, 3> Channels;

Image
custom(const Image &src_image, const Matrix<double> &kernel);

Image
gaussian(const Image &src_image, const double sigma, const int radius);
Image
gaussian_separable(const Image &src_image, const double sigma, const int radius);

Image
mirror(const Image &src_image, const Matrix<double> &kernel);
Image
mirror(const Image &src_image, const uint dim_rows, const uint dim_cols);

Image
sobel_x(const Image &src_image);
Image
sobel_y(const Image &src_image);
Image
canny(const Image &src_image, int thresh1, int thresh2);
Matrix<double>
nonmax_suppr(const Matrix<double> &grad, const Matrix<double> &theta);
Matrix<uint>
thresh(const Matrix<double> &grad, const double &thresh1, const double &thresh2);

Image
align(const Image &src_image, const std::string postprocessing="None");
Image
align(const Image &src_image, const std::string &postprocessing, double fraction);
Image
align(const Image &src_image, const std::string &postprocessing, int k);
Image
clipper(const Image& src_image, const Image &border, const double frac);
Channels
extract_channels(const Image &target);
double
mse(const Matrix<double> &x, const Matrix<double> &y);
std::array<int, 2>
align_with_mse(const Channels &m, const Channels &s, int chan_m, int chan_s, const uint range=15);
std::array<int, 2>
align_with_cc(const Channels &m, const Channels &s, int chan_m, int chan_s);
Channels
image2channels(const Image &target);
Image
channels2image(const Channels &c);
Channels
rel_align(const Channels &m, const Channels &s, int chan_m, int chan_s, std::array<int, 2> relcoords);
uint
longest_side(const Channels &a, const Channels &b, const Channels &c);
uint
longest_side(const Image &a, const Image &b, const Image &c);

Image
gray_world(const Image &src_image);
Image
unsharp(const Image &src_image);
Image
autocontrast(const Image &src_image, const double fraction);

Image
resize(const Image &src_image, double scale);
Channels
resize(const Channels &c, double scale);

int main(int argc, char **argv)
{
    try {
        check_argc(argc, 2);
        if (string(argv[1]) == "--help") {
            print_help(argv[0]);
            return 0;
        }

        check_argc(argc, 4);
        Image src_image = load_image(argv[1]), dst_image;

        string action(argv[3]);

        if (action == "--sobel-x") {
            check_argc(argc, 4, 4);
            dst_image = sobel_x(src_image);
        } else if (action == "--sobel-y") {
            check_argc(argc, 4, 4);
            dst_image = sobel_y(src_image);
        } else if (action == "--unsharp") {
            check_argc(argc, 4, 4);
            dst_image = unsharp(src_image);
        } else if (action == "--gray-world") {
            check_argc(argc, 4, 4);
            dst_image = gray_world(src_image);
        } else if (action == "--mirror") {
            check_argc(argc, 4, 4);
            const auto rows = src_image.n_rows;
            const auto cols = src_image.n_cols;
            dst_image = mirror(src_image, rows, cols);
        } else if (action == "--resize") {
            check_argc(argc, 5, 5);
            double scale = read_value<double>(argv[4]);
            dst_image = resize(src_image, scale);
        }  else if (action == "--custom") {
            check_argc(argc, 5, 5);
            Matrix<double> kernel = parse_kernel(argv[4]);
            // Function custom is useful for making concrete linear filtrations
            // like gaussian or sobel. So, we assume that you implement custom
            // and then implement concrete filtrations using this function.
            // For example, sobel_x may look like this:
            // sobel_x (...) {
            //    Matrix<double> kernel = {{-1, 0, 1},
            //                             {-2, 0, 2},
            //                             {-1, 0, 1}};
            //    return custom(src_image, kernel);
            // }
            dst_image = custom(src_image, kernel);
        } else if (action == "--autocontrast") {
            check_argc(argc, 4, 5);
            double fraction = 0.0;
            if (argc == 5) {
                fraction = read_value<double>(argv[4]);
                check_number("fraction", fraction, 0.0, 0.4);
            }
            dst_image = autocontrast(src_image, fraction);
        } else if (action == "--gaussian" || action == "--gaussian-separable") {
            check_argc(argc, 5, 6);
            double sigma = read_value<double>(argv[4]);
            check_number("sigma", sigma, 0.1, 100.0);
            int radius = 3 * sigma;
            if (argc == 6) {
                radius = read_value<int>(argv[5]);
                check_number("radius", radius, 1);
            }
            if (action == "--gaussian") {
                dst_image = gaussian(src_image, sigma, radius);
            } else {
                dst_image = gaussian_separable(src_image, sigma, radius);
            }
        } else if (action == "--canny") {
            check_argc(6, 6);
            int threshold1 = read_value<int>(argv[4]);
            check_number("threshold1", threshold1, 0, 360);
            int threshold2 = read_value<int>(argv[5]);
            check_number("threshold2", threshold2, 0, 360);
            if (threshold1 >= threshold2)
                throw string("threshold1 must be less than threshold2");
            dst_image = canny(src_image, threshold1, threshold2);
        } else if (action == "--align") {
            check_argc(argc, 4, 6);
            if (argc == 5 || argc == 6) {
                string postprocessing(argv[4]);
                if (postprocessing == "--gray-world" ||
                    postprocessing == "--unsharp") {
                    check_argc(argc, 5, 5);
                    dst_image = align(src_image, postprocessing);
                } else if (postprocessing == "--autocontrast") {
                    double fraction = 0.0;
                    if (argc == 6) {
                        fraction = read_value<double>(argv[5]);
                        check_number("fraction", fraction, 0.0, 0.4);
                    }
                    dst_image = align(src_image, postprocessing, fraction);
                } else if (postprocessing == "--subpixel") {
                    int k = 1;
                    if (argc == 6) {
                        k = read_value<int>(argv[5]);
                        check_number("k pixels", k, 1, 8);
                    }
                    dst_image = align(src_image, postprocessing, k);
                } else {
                    throw string("unknown align option ") + postprocessing;
                }
            } else {
                dst_image = align(src_image);
            }
        } else {
            throw string("unknown action ") + action;
        }
        save_image(dst_image, argv[2]);
    } catch (const string &s) {
        cerr << "Error: " << s << endl;
        cerr << "For help type: " << endl << argv[0] << " --help" << endl;
        return 1;
    }
}

Image
mirror(const Image &src_image, const uint dim_rows, const uint dim_cols)
{
    // implement mirroring
    const auto rows = src_image.n_rows;
    const auto cols = src_image.n_cols;

    // if (krows % 2 != 1 || kcols % 2 != 1) {
    //     std::string msg = "krows or kcols aren't odd";
    //     throw msg;
    // }

    const auto hrows = dim_rows;
    const auto hcols = dim_cols;

    if (hrows > rows || hcols > cols) {
        std::string msg = "too much mirroring requested";
        throw msg;
    }

    auto new_rows =
        src_image.n_rows + 2 * hrows;
    auto new_cols =
        src_image.n_cols + 2 * hcols;

    Image image(new_rows, new_cols);

    // copy center
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            image(hrows + i, hcols + j) = 
                src_image(i, j);
        }
    }

    // upper nineth
    for (uint i = 0; i < hrows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            image(hrows - 1 - i, hcols + j) =
                src_image(i, j);
        }
    }

    // lower nineth
    for (uint i = 0; i < hrows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            image(hrows + rows + i, hcols + j) =
                src_image(rows - 1 - i, j);
        }
    }

    // left part
    for (uint i = 0; i < new_rows; ++i) {
        for (uint j = 0; j < hcols; ++j) {
            image(i, hcols - 1 - j) = image(i, hcols + j);
        }
    }

    // right part
    for (uint i = 0; i < new_rows; ++i) {
        for (uint j = 0; j < hcols; ++j) {
            image(i, hcols + cols + j) =
                image(i, hcols + cols - 1 - j);
        }
    }

    return image;
}

Image
mirror(const Image &src_image, const Matrix<double> &kernel)
{
    return mirror(src_image, kernel.n_rows / 2, kernel.n_cols / 2);
}

Image
custom(const Image &src_image, const Matrix<double> &kernel)
{
    Image image = mirror(src_image, kernel);

    FilterOp filter(kernel);
    image = image.unary_map(filter);

    const auto rows = src_image.n_rows;
    const auto cols = src_image.n_cols;
    const auto hrows = kernel.n_rows / 2;
    const auto hcols = kernel.n_cols / 2;

    Image dst_image(rows, cols);
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            dst_image(i, j) = image(hrows + i, hcols + j);
        }
    }

    return dst_image;
}

Matrix<double>
gaussian_kernel(const double sigma, const int radius, std::string which)
{
    using std::pow;
    using std::abs;
    using std::exp;

    auto size = 2 * radius + 1;
    long double cumsum = 0;
    if (which == "matrix") {
        Matrix<double> kernel(size, size);
        for (auto i = 0; i < size; ++i) {
            for (auto j = 0; j < size; ++j) {
                auto dist2 = pow(i - radius, 2) + pow(j - radius, 2);
                kernel(i, j) = exp(-dist2 / 2 / pow(sigma, 2));
                cumsum += kernel(i, j);
            }
        }

        // now normalize the kernel, bitte!
        for (auto i = 0; i < size; ++i) {
            for (auto j = 0; j < size; ++j) {
                kernel(i, j) /= cumsum;
            }
        }
    
        return kernel;
    } else if (which == "vector") {
        Matrix<double> kernel(1, size);
        for (auto i = 0; i < size; ++i) {
            auto dist2 = pow(i - radius, 2);
            kernel(0, i) = exp(-dist2 / 2 / pow(sigma, 2));
            cumsum += kernel(0, i);
        }

        // now normalize the kernel, bitte!
        for (auto i = 0; i < size; ++i) {
                kernel(0, i) /= cumsum;
        }
    
        return kernel;
    } else {
        std::string msg = "Which gaussian kernel do you want?";
        throw msg;
    }
}

Image
gaussian_separable(const Image &src_image, const double sigma, const int radius)
{
    using std::make_tuple;
    // we will be needing just the first (or last) row (or column).
    Matrix<double> kernel = gaussian_kernel(sigma, radius, "vector");
    
    Image image = mirror(src_image, radius, radius);

    const auto rows = src_image.n_rows;
    const auto cols = src_image.n_cols;
    // here is the tricky bit: second is `kernel.n_rows` for a reason:
    // even though the kernel is a 1 x n vector, it will be applied
    // like an ordinary square matrix;
    const auto hrows = kernel.n_cols / 2;
    const auto hcols = kernel.n_cols / 2;

    Image result(rows, cols);

    // traverse image horizontally, store results in result
    const auto kcols = kernel.n_cols;
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            double r, g, b;
            const auto row = hrows + i;
            double sum_r = 0, sum_g = 0, sum_b = 0;
            for (uint k = 0; k < kcols; ++k) {
                std::tie(r, g, b) = image(row, j + k);
                sum_r += r * kernel(0, k);
                sum_g += g * kernel(0, k);
                sum_b += b * kernel(0, k);
            }
            result(i, j) = make_tuple(sum_r, sum_g, sum_b);
        }
    }

    image = mirror(result, radius, radius);

    // traverse image vertically, store results in result
    for (uint i = 0; i < cols; ++i) {
        for (uint j = 0; j < rows; ++j) {
            double r, g, b;
            const auto col = hcols + i;
            double sum_r = 0, sum_g = 0, sum_b = 0;
            for (uint k = 0; k < kcols; ++k) {
                std::tie(r, g, b) = image(j + k, col);
                sum_r += r * kernel(0, k);
                sum_g += g * kernel(0, k);
                sum_b += b * kernel(0, k);
            }
            result(j, i) = make_tuple(sum_r, sum_g, sum_b);
        }
    }

    return result;
}


Image
gaussian(const Image &src_image, const double sigma, const int radius)
{
    Matrix<double> kernel = gaussian_kernel(sigma, radius, "matrix");

    return custom(src_image, kernel);
}

Image
sobel_x(const Image &src_image)
{
    Matrix<double> kernel = {
        {-1, 0, 1},
        {-2, 0, 2},
        {-1, 0, 1}
    };

    return custom(src_image, kernel);
}

Image
sobel_y(const Image &src_image)
{
    Matrix<double> kernel = {
        {1, 2, 1},
        {0, 0, 0},
        {-1, -2, -1}
    };
    
    return custom(src_image, kernel);
}

Image
align(const Image &src_image, const std::string postprocessing)
{
    const auto rows3 = src_image.n_rows / 3;
    const auto cols3 = src_image.n_cols;

    std::array<Image, 3> slices;
    // deliberately ignore submatrix -- it's such an evil function!
    // if one uses submatrix and then, for some reason, has to
    // manipulate with Image by hand, he will most likely miss the
    // desired region!
    for (uint i = 0; i < 3; ++i) {
        slices[i] = src_image.submatrix(i * rows3, 0, rows3, cols3);
    }
   
    // let's take some up-the-sleeve values
    const int thresh1 = 60;
    const int thresh2 = 280;

    std::array<Image, 3> borders;
    for (uint i = 0; i < 3; ++i) {
        borders[i] = canny(slices[i], thresh1, thresh2);
    }

    // another up-my-sleeve value
    const double fraction = 0.20;
    std::array<Image, 3> clipped;
    for (uint i = 0; i < 3; ++i) {
        clipped[i] = clipper(slices[i], borders[i], fraction);
    }

    const double scale = 0.5;
    const unsigned int MINPXSIZE = 400;

    unsigned int steps = 1;
    std::vector<Image> small_copies =  {
        clipped[0], clipped[1], clipped[2]
    };

    while (longest_side(small_copies[3 * (steps - 1)],
                        small_copies[3 * (steps - 1) + 1],
                        small_copies[3 * (steps - 1) + 2]) > MINPXSIZE) {
        for (auto i = 0; i < 3; ++i) {
            small_copies.push_back(resize(small_copies[3 * (steps - 1) + i], scale));
        }
        
        ++steps;
    }

    Channels top = image2channels(small_copies[3 * (steps - 1)]);
    Channels mid = image2channels(small_copies[3 * (steps - 1) + 1]);
    Channels bot = image2channels(small_copies[3 * (steps - 1) + 2]);

    std::array<int, 2> relcoords_tm;
    std::array<int, 2> relcoords_tmb;
    relcoords_tm =
        align_with_mse(top, mid, 2, 1, 30);
    Channels BlueGreen =
        rel_align(top, mid, 2, 1, relcoords_tm);
    relcoords_tmb =
        align_with_mse(bot, BlueGreen, 0, 1, 30);
    Channels RedGreenBlue =
        rel_align(bot, BlueGreen, 0, 1, relcoords_tmb);
    
    for (uint i = 1; i != steps; ++i) {
        relcoords_tm[0] = round(relcoords_tm[0] / scale);
        relcoords_tm[1] = round(relcoords_tm[1] / scale);
        relcoords_tmb[0] = round(relcoords_tmb[0] / scale);
        relcoords_tmb[1] = round(relcoords_tmb[1] / scale);

        top = image2channels(small_copies[3 * (steps - 1 - i)]);
        mid = image2channels(small_copies[3 * (steps - 1 - i) + 1]);
        bot = image2channels(small_copies[3 * (steps - 1 - i) + 2]);

        BlueGreen = 
            rel_align(top, mid, 2, 1, relcoords_tm);
        RedGreenBlue =
            rel_align(bot, BlueGreen, 0, 1, relcoords_tmb);

        relcoords_tm =
            align_with_mse(top, mid, 2, 1, 2);
        relcoords_tmb =
            align_with_mse(bot, BlueGreen, 0, 1, 2);
    }    

    Image result = channels2image(RedGreenBlue);

    if (postprocessing == "--unsharp") {
        result = unsharp(result);
    } else if (postprocessing == "--gray-world") {
        result = gray_world(result);
    }

    return result;
}

Image
align(const Image &src_image, const std::string &postprocessing, double fraction)
{
    Image result = align(src_image);
    
    if (postprocessing == "--autocontrast") {
        result = autocontrast(result, fraction);
    } else {
        std::string msg = "bad postprocessing option, expected `autocontrast`";
        throw msg;
    }

    return result;
}

Image
align(const Image &src_image, const std::string &postprocessing, int k)
{
    Image result = resize(src_image, k);

    result = align(src_image);

    return resize(result, 1.0 / k);
}

uint
longest_side(const Channels &a, const Channels &b, const Channels &c)
{
    using std::max;
    const auto amax = max(a[0].n_rows, a[0].n_cols);
    const auto bmax = max(b[0].n_rows, b[0].n_cols);
    const auto cmax = max(c[0].n_rows, c[0].n_cols);
    return max(max(amax, bmax), cmax);
}

uint
longest_side(const Image &a, const Image &b, const Image &c)
{
    using std::max;
    const auto amax = max(a.n_rows, a.n_cols);
    const auto bmax = max(b.n_rows, b.n_cols);
    const auto cmax = max(c.n_rows, c.n_cols);
    return max(max(amax, bmax), cmax);
}

Image
channels2image(const Channels &c)
{
    if (c[0].n_rows != c[1].n_rows || c[0].n_rows != c[2].n_rows
        || c[0].n_cols != c[1].n_cols || c[0].n_cols != c[2].n_cols) {
        std::string msg = "channels have mixed dimensions";
        throw msg;
    }

    using std::make_tuple;
    const auto rows = c[0].n_rows;
    const auto cols = c[0].n_cols;
    Image result(rows, cols);
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            result(i, j) = 
                make_tuple(c[0](i, j), c[1](i, j), c[2](i, j));
        }
    }

    return result;
}

Channels
image2channels(const Image &target)
{
    const auto rows = target.n_rows;
    const auto cols = target.n_cols;
    
    std::array<Matrix<double>, 3> result = {
        Matrix<double>(rows, cols),
        Matrix<double>(rows, cols),
        Matrix<double>(rows, cols)
    };

    using std::get;
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            result[0](i, j) = get<0>(target(i, j));
            result[1](i, j) = get<1>(target(i, j));
            result[2](i, j) = get<2>(target(i, j));
        }
    }

    return result;
}

double
cross_correlation(const Matrix<double> &x, const Matrix<double> &y)
{
    if (x.n_rows != y.n_rows || x.n_cols != y.n_cols) {
        std::string msg = "mse will not run when dimensions mismatch";
        throw msg;
    } else if (x.n_rows == 0 || x.n_cols == 0 || y.n_rows == 0 || y.n_cols == 0) {
        std::string msg = "this is a zero-matrix surprise :(";
        throw msg;
    }

    const auto rows = x.n_rows;
    const auto cols = y.n_cols;

    double result = 0;
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            auto a = x(i, j);
            auto b = y(i, j);
            result += a * b;
        }
    }

    return result;
}

double
mse(const Matrix<double> &x, const Matrix<double> &y)
{
    if (x.n_rows != y.n_rows || x.n_cols != y.n_cols) {
        std::string msg = "mse will not run when dimensions mismatch";
        throw msg;
    } else if (x.n_rows == 0 || x.n_cols == 0 || y.n_rows == 0 || y.n_cols == 0) {
        std::string msg = "this is a zero-matrix surprise :(";
        throw msg;
    }

    using std::pow;
    const auto rows = x.n_rows;
    const auto cols = x.n_cols;

    double result = 0;
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            auto a = x(i, j);
            auto b = y(i, j);
            result += pow(a - b, 2);
        }
    }
    result = result / rows / cols;

    return result;
}

std::array<uint, 6>
rel2abs(int i, int j, uint m_rows, uint m_cols, uint s_rows, uint s_cols)
{
    using std::min;
    const uint mpinr = i > 0 ? 0 : -i;
    const uint mpinc = j > 0 ? 0 : -j;
    const uint mrows = m_rows - mpinr;
    const uint mcols = m_cols - mpinc;
            
    const uint spinr = i > 0 ? i : 0;
    const uint spinc = j > 0 ? j : 0;
    const uint srows = s_rows - spinr;
    const uint scols = s_cols - spinc;

    const uint rows = min(mrows, srows);
    const uint cols = min(mcols, scols);

    std::array<uint, 6> result = {
        mpinr, mpinc,
        spinr, spinc,
        rows, cols
    };

    return result;
}

// typedef std::array<Matrix<double>, 3> Channels
Channels
rel_align(const Channels &m, const Channels &s, int chan_m, int chan_s, std::array<int, 2> relcoords)
{

    if (chan_m == chan_s) {
        std::string msg =
            "Attempted to real_align matching channels, abort.";
        throw msg;
    }

    int chan_p;
    if ((chan_m == 0 && chan_s == 1) || (chan_m == 1 && chan_s == 0)) {
        chan_p = 2;
    } else if ((chan_m == 0 && chan_s == 2) || (chan_m == 2 && chan_s == 0)) {
        chan_p = 1;
    } else {
        chan_p = 0;
    }
    
    std::array<uint, 6> abscoords =
        rel2abs(relcoords[0],
                relcoords[1],
                m[chan_m].n_rows,
                m[chan_m].n_cols,
                s[chan_s].n_rows,
                s[chan_s].n_cols);

    const auto mpinr = abscoords[0];
    const auto mpinc = abscoords[1];
    const auto spinr = abscoords[2];
    const auto spinc = abscoords[3];
    const auto rows = abscoords[4];
    const auto cols = abscoords[5];
   
    Channels result = {
        Matrix<double>(rows, cols),
        Matrix<double>(rows, cols),
        Matrix<double>(rows, cols)
    };

    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            result[chan_m](i, j) = m[chan_m](i + mpinr, j + mpinc);
        }
    }

    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            result[chan_p](i, j) = s[chan_p](i + spinr, j + spinc);
        }
    }

    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            result[chan_s](i, j) = s[chan_s](i + spinr, j + spinc);
        }
    }

    return result;
}

std::array<int, 2>
align_with_cc(const Channels &m, const Channels &s, int chan_m, int chan_s)
{
   // let's take 15 px ranges [-15, 15] as suggested by task
    const auto range = 15;

    double max_metric = 0;
    int iMax = 0; int jMax = 0;
    for (auto i = -range; i != range; ++i) {
        for (auto j = -range; j != range; ++j) {
            std::array<uint, 6> abscoords =
                rel2abs(i, j,
                        m[chan_m].n_rows,
                        m[chan_m].n_cols,
                        s[chan_s].n_rows,
                        s[chan_s].n_cols);

            const auto mpinr = abscoords[0];
            const auto mpinc = abscoords[1];
            const auto spinr = abscoords[2];
            const auto spinc = abscoords[3];
            const auto rows = abscoords[4];
            const auto cols = abscoords[5];

            auto msub = m[chan_m].submatrix(mpinr, mpinc, rows, cols);
            auto ssub = s[chan_s].submatrix(spinr, spinc, rows, cols);

            auto metric_val = cross_correlation(msub, ssub);
            if (metric_val > max_metric) {
                max_metric = metric_val;
                iMax = i; jMax = j;
            }
        }
    }

    std::array<int, 2> result = {iMax, jMax};
    return result;
}

std::array<int, 2>
align_with_mse(const Channels &m, const Channels &s, int chan_m, int chan_s, const uint range)
{
    double min_metric = 1e20; // std::numeric_limits infinity behaves weird
    int iMin = 0; int jMin = 0;
    for (auto i = -range; i != range; ++i) {
        for (auto j = -range; j != range; ++j) {
            std::array<uint, 6> abscoords =
                rel2abs(i, j,
                        m[chan_m].n_rows,
                        m[chan_m].n_cols,
                        s[chan_s].n_rows,
                        s[chan_s].n_cols);

            const auto mpinr = abscoords[0];
            const auto mpinc = abscoords[1];
            const auto spinr = abscoords[2];
            const auto spinc = abscoords[3];
            const auto rows = abscoords[4];
            const auto cols = abscoords[5];

            auto msub = m[chan_m].submatrix(mpinr, mpinc, rows, cols);
            auto ssub = s[chan_s].submatrix(spinr, spinc, rows, cols);

            auto metric_val = mse(msub, ssub);
            if (metric_val < min_metric) {
                min_metric = metric_val;
                iMin = i; jMin = j;
            }
        }
    }

    std::array<int, 2> result = {iMin, jMin};
    return result;
}

Image
canny(const Image &src_image, int thresh1, int thresh2)
{
    // please keep in mind that src_image is in grayscale
    // take sigma 1.4 and radius 2 from problem statement
    const double sigma = 1.4;
    const int radius = 2;

    using std::pow;
    using std::make_tuple;
    const uint rows = src_image.n_rows;
    const uint cols = src_image.n_cols;

    Image image = gaussian_separable(src_image, sigma, radius);

    Image Ix = sobel_x(image);
    Image Iy = sobel_y(image);

    Matrix<double> G(rows, cols);
    Matrix<double> Theta(rows, cols);
    double rx, gx, bx, ry, gy, by;
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            std::tie(rx, gx, bx) = Ix(i, j);
            std::tie(ry, gy, by) = Iy(i, j);
            G(i, j) = std::sqrt(rx * rx + ry * ry);
            Theta(i, j) = atan2(ry, rx);
        }
    }

    G = nonmax_suppr(G, Theta);
    // the result of nonmax_suppr is a map with -1 or grad(i, j)

    Matrix<uint> border = thresh(G, thresh1, thresh2);
    // the result of border is a map with 0, 127, 255;

    // Warning: an edge may contain strong pixels which will propagate
    // to the resulting map which in turn will make that map different
    // from what you have in tests. Please think on that, I will mention
    // this problem in russian in my email. Four loops below is a gentoo
    // test trick.
    const int IGNORE = 1;
    for (uint i = 0; i < IGNORE; ++i) {
        for (uint j = 0; j < cols; ++j) {
            border(i, j) = 0;
        }
    }
    for (uint i = rows - IGNORE; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            border(i, j) = 0;
        }
    }
    for (uint j = 0; j < IGNORE; ++j) {
        for (uint i = 0; i < rows; ++i) {
            border(i, j) = 0;
        }
    }
    for (uint j = cols - IGNORE; j < cols; ++j) {
        for (uint i = 0; i < rows; ++i) {
            border(i, j) = 0;
        }
    }

    // Make a list of strong pixels
    std::vector<std::array<uint, 2>> sp;
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            if (border(i, j) == 255) {
                sp.push_back({i, j});
            }
        }
    }

    // x is rowcoord
    const std::vector<int> xrel = {-1, -1, -1,  0, +1, +1, +1,  0};
    // y is colcoord
    const std::vector<int> yrel = {-1,  0, +1, +1, +1,  0, -1, -1};    

    std::queue<std::array<uint, 2>> Queued;
    for (auto i = sp.begin(); i < sp.end(); ++i) {
        const auto spixel = *i;
        Queued.push(spixel);
        while (not Queued.empty()) {
            const auto next = Queued.front();
            for (uint j = 0; j < xrel.size(); ++j) {
                const auto x = next[0] + xrel[j];
                const auto y = next[1] + yrel[j];
                if (x != UINT_MAX && x != rows
                    && y != UINT_MAX && y != cols
                    && border(x, y) == 127) {
                    border(x, y) = 255;
                    Queued.push({x, y});
                }
            }
            Queued.pop();
        }
    }

    using std::make_tuple;
    Image result(border.n_rows, border.n_cols);
    for (uint i = 0; i < border.n_rows; ++i) {
        for (uint j = 0; j < border.n_cols; ++j) {
            result(i, j) = make_tuple(border(i, j) == 255 ? 255 : 0,
                                      border(i, j) == 255 ? 255 : 0,
                                      border(i, j) == 255 ? 255 : 0);
        }
    }
    
    return result;
}

Matrix<uint>
thresh(const Matrix<double> &grad,
       const double &thresh1, const double &thresh2)
{
    // let's create a preliminary border map;
    // it'll have the following format:
    // 0 -- pixel is y no means a border
    // 127 -- we aren't yet sure about the pixel
    // 255 -- the pixel is definitely a border
    const auto rows = grad.n_rows;
    const auto cols = grad.n_cols;
    Matrix<uint> result(rows, cols);
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            if (grad(i, j) > 0) {
                if (grad(i, j) < thresh1) {
                    result(i, j) = 0;
                } else if (grad(i, j) > thresh2) {
                    result(i, j) = 255;
                } else {
                    // who knows...
                    result(i, j) = 127;
                }
            } else {
                // grad(i, j) < 0 which means we have to suppress
                // anyway -- this is what nonmax_suppr insists on
                result(i, j) = 0;
            }
        }
    }

    return result;
}

Matrix<double>
nonmax_suppr(const Matrix<double> &grad, const Matrix<double> &theta)
{
    const auto rows = grad.n_rows;
    const auto cols = grad.n_cols;

    Matrix<double> result(rows, cols);

    const double alpha = M_PI / 8;
    for (uint i = 1; i < rows - 1; ++i) {
        for (uint j = 1; j < cols - 1; ++j) {
            double t = theta(i, j);
            double neighb1, neighb2;
            if ((-alpha <= t && t <= alpha)
                || (7 * alpha <= t && t <= M_PI)
                || (-M_PI <= t && t <= -7 * alpha)) {
                neighb1 = grad(i, j + 1);
                neighb2 = grad(i, j - 1);
            } else if ((alpha <= t && t <= 3 * alpha)
                       || (-7 * alpha <= t && t <= -5 * alpha)) {
                neighb1 = grad(i - 1, j + 1);
                neighb2 = grad(i + 1, j - 1);
            } else if ((3 * alpha <= t && t <= 5 * alpha)
                       || (-5 * alpha <= t && t <= -3 * alpha)) {
                neighb1 = grad(i - 1, j);
                neighb2 = grad(i + 1, j);
            } else if ((5 * alpha <= t && t <= 7 * alpha)
                       || (-3 * alpha <= t && t <= -alpha)) {
                neighb1 = grad(i - 1, j - 1);
                neighb2 = grad(i + 1, j + 1);
            } else {
                std::string msg =
                    "the way you suppress nonmax grads stinks";
                throw msg;
            }
            if (grad(i, j) > neighb1 && grad(i, j) > neighb2) {
                result(i, j) = grad(i, j);
            } else {
                // mark definitely suppressed pixels
                result(i, j) = -1;
            }
        }
    }

    // now only pixels right on the border are unknown.  since the
    // actual `border` of the image is most likely further, let's mark
    // border pixels as 0

    // when iterating, stay cache-friendly
    for (uint i = 0; i < cols; ++i) {
        result(0, i) = 0;
    }
    
    for (uint i = 0; i < cols; ++i) {
        result(rows - 1, i) = 0;
    }

    // even when we're talkig rows :) 
    for (uint i = 0; i < rows; ++i) {
        result(i, 0) = 0;
    }

    for (uint i = 0; i < rows; ++i) {
        result(i, cols - 1) = 0;
    }

    return result;
}

Image
gray_world(const Image &src_image)
{
    const auto rows = src_image.n_rows;
    const auto cols = src_image.n_cols;

    if (rows == 0 or cols == 0) {
        std::string msg =
            "src_image: rows == 0 or cols == 0, gray_world aborts";
        throw msg;
    }

    Image result(rows, cols);

    double r, g, b;
    long double Sr = 0, Sg = 0, Sb = 0, S = 0;
    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            std::tie(r, g, b) = src_image(i, j);
            Sr += r;
            Sg += g;
            Sb += b;
        }
    }

    Sr = Sr / rows / cols;
    Sg = Sg / rows / cols;
    Sb = Sb / rows / cols;

    S = (Sr + Sg + Sb) / 3;

    const auto eps = 1e-10;
    // if (Sr < eps || Sg < eps || Sb < eps) {
    //     std::string msg = "One of the image channels is pitch black";
    //     throw msg;
    // }

    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            std::tie(r, g, b) = src_image(i, j);
            r = (Sr < eps) ? S : r * S / Sr;
            g = (Sg < eps) ? S : g * S / Sg;
            b = (Sb < eps) ? S : b * S / Sb;
            result(i, j) = std::make_tuple(r, g, b);
        }
    }
    
    return result;
}

Image
unsharp(const Image &src_image)
{
    Matrix<double> kernel = {
        {-1.0/6, -2.0/3, -1.0/6},
        {-2.0/3, 4 + 1.0/3, -2.0/3},
        {-1.0/6, -2.0/3, -1.0/6}
    };

    return custom(src_image, kernel);
}

Image
autocontrast(const Image &src_image, const double fraction)
{
    const auto rows = src_image.n_rows;
    const auto cols = src_image.n_cols;

    Image result(rows, cols);
    std::vector<double> vluminances(rows * cols);

    double r, g, b;
    for (unsigned int i = 0; i < rows; ++i) {
        for (unsigned int j = 0; j < cols; ++j) {
            std::tie(r, g, b) = src_image(i, j);
            const auto lum =
                round(0.2125 * r + 0.7154 * g + 0.0721 * b);
            vluminances[i * rows + j] = lum;
        }
    }

    const int halfway = round(cols * rows * fraction);

    std::nth_element(vluminances.begin(),
                     vluminances.end() - 1 - halfway,
                     vluminances.end());

    std::nth_element(vluminances.begin(),
                     vluminances.begin() + halfway,
                     vluminances.end());

    const auto y_min = vluminances[halfway];
    const auto y_max = vluminances[vluminances.size() - 1 - halfway];
    const auto delta = y_max - y_min;

    const auto eps = 1e-7;
    if (delta < eps) {
        std::cerr << "image is a single color, will do no autocontrast." << std::endl;
        return src_image;
    }

    for (unsigned int i = 0; i < rows; ++i) {
        for (unsigned int j = 0; j < cols; ++j) {
            std::tie(r, g, b) = src_image(i, j);

            r = 255 * (r - y_min) / delta;
            g = 255 * (g - y_min) / delta;
            b = 255 * (b - y_min) / delta;

            result(i, j) = std::make_tuple(r, g, b);
        }
    }

    return result;
}

Channels
resize(const Channels &c, double scale)
{
    Image result = channels2image(c);
    result = resize(result, scale);
    return image2channels(result);
}

double linear(const double a, const double b, const double x)
{
    return (b - a) * x + a;
}

Image
resize(const Image &src_image, double scale)
{
    using std::get;
    const int cols = src_image.n_cols;
    const int rows = src_image.n_rows;
    const int new_rows = src_image.n_rows * scale;
    const int new_cols = src_image.n_cols * scale;

    Image result(new_rows, new_cols);
    for (auto i = 0; i < new_rows; ++i) {
        const auto k = i / scale;
        for (auto j = 0; j < new_cols; ++j) {
            const auto l = j / scale;
            const int y1 = floor(k);
            const auto y2 = y1 + 1 == rows ? y1 : y1 + 1;

            const int x1 = floor(l);
            const auto x2 = x1 + 1 == cols ? x1 : x1 + 1;

            const auto dy = k - y1;
            const auto dx = l - x1;

            const auto Q11 = src_image(y1, x1);
            const auto Q12 = src_image(y1, x2);
            const auto Q21 = src_image(y2, x1);
            const auto Q22 = src_image(y2, x2);

            const auto r =
                linear(linear(get<0>(Q11), get<0>(Q12), dx),
                       linear(get<0>(Q21), get<0>(Q22), dx), dy);

            const auto g =
                linear(linear(get<1>(Q11), get<1>(Q12), dx),
                       linear(get<1>(Q21), get<1>(Q22), dx), dy);

            const auto b =
                linear(linear(get<2>(Q11), get<2>(Q12), dx),
                       linear(get<2>(Q21), get<2>(Q22), dx), dy);

            result(i, j) = std::make_tuple(r, g, b);
        }
    }
    return result;
}

std::array<uint, 2>
find_2_maxima(std::vector<double> data)
{
    std::array<uint, 2> result;
    const auto size = data.size();

    for (auto j = 0; j < 2; ++j) {
        uint iMax = 0;
        for (uint i = 1; i < size; ++i) {
            if (data[iMax] < data[i]) {
                iMax = i;
            }
        }

        result[j] = iMax;
        data[iMax] = 0;
        if (iMax > 0) {
            data[iMax - 1] = 0;
        }
        if (iMax < size - 1) {
            data[iMax + 1] = 0;
        }
    }

    return result;
}

Image
clipper(const Image& src_image, const Image &border, const double frac)
{
    using std::get;
    // keep in mind that border is a special type of Image...
    const auto rows = src_image.n_rows;
    const auto cols = src_image.n_cols;

    unsigned int frows = rows * frac;
    unsigned int fcols = cols * frac;

    std::vector<double> colsums(fcols);
    std::fill(colsums.begin(), colsums.end(), 0);

    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < fcols; ++j) {
            colsums[j] += get<0>(border(i, j));
        }
    }
    
    std::array<uint, 2> left_coords = find_2_maxima(colsums);
    std::fill(colsums.begin(), colsums.end(), 0);

    for (uint i = 0; i < rows; ++i) {
        for (uint j = 0; j < fcols; ++j) {
            colsums[fcols - 1 - j] += get<0>(border(i, cols - 1 - j));
        }
    }

    std::array<uint, 2> right_coords = find_2_maxima(colsums);

    std::vector<double> rowsums(frows);
    std::fill(rowsums.begin(), rowsums.end(), 0);

    for (uint i = 0; i < frows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            rowsums[i] += get<0>(border(i, j));
        }
    }

    std::array<uint, 2> top_coords = find_2_maxima(rowsums);
    std::fill(rowsums.begin(), rowsums.end(), 0);

    for (uint i = 0; i < frows; ++i) {
        for (uint j = 0; j < cols; ++j) {
            rowsums[frows - 1 - i] += get<0>(border(rows - 1 - i, j));
        }
    }

    std::array<uint, 2> bottom_coords = find_2_maxima(rowsums);

    right_coords[0] += cols - fcols;
    right_coords[1] += cols - fcols;
    bottom_coords[0] += rows - frows;
    bottom_coords[1] += rows - frows;

    using std::max;
    using std::min;
    auto left = max(left_coords[0], left_coords[1]);
    auto top = max(top_coords[0], top_coords[1]);
    auto right = min(right_coords[0], right_coords[1]);
    auto bottom = min(bottom_coords[0], bottom_coords[1]);

    Image result(bottom - top + 1, right - left + 1);
    for (uint i = top; i <= bottom; ++i) {
        for (uint j = left; j <= right; ++j) {
            result (i - top, j - left) = src_image(i, j);
        }
    }

    return result;
}
