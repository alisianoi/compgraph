#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <initializer_list>
#include <limits>
#include <tuple>

using std::string;
using std::stringstream;
using std::cout;
using std::cerr;
using std::endl;
using std::numeric_limits;

#include "io.h"
#include "matrix.h"
#include "filter.h"

void print_help(const char *argv0)
{
    const char *usage =
R"(where PARAMS are from list:

--align [--bicubic-interp] [--gray-world | --unsharp | --autocontrast [<fraction>]]
    align images with one of postprocessing functions

--gaussian <sigma> [<radius>=1]
    gaussian blur of image, 0.1 < sigma < 100, radius = 1, 2, ...

--gaussian-separable <sigma> [<radius>=1]
    same, but gaussian is separable

--sobel-x
    Sobel x derivative of image

--sobel-y
    Sobel y derivative of image

--unsharp
    sharpen image

--gray-world
    gray world color balancing

--autocontrast [<fraction>=0.0]
    autocontrast image. <fraction> of pixels must be croped for robustness

--resize <scale>
    resize image with factor scale. scale is real number > 0

--canny <threshold1> <threshold2>
    apply Canny filter to grayscale image. threshold1 < threshold2,
    both are in 0..360

--custom <kernel_string>
    convolve image with custom kernel, which is given by kernel_string, example:
    kernel_string = '1,2,3;4,5,6;7,8,9' defines kernel of size 3

[<param>=default_val] means that parameter is optional.
)";
    cout << "Usage: " << argv0 << " <input_image_path> <output_image_path> "
         << "PARAMS" << endl;
    cout << usage;
}

template<typename ValueType>
ValueType read_value(string s)
{
    stringstream ss(s);
    ValueType res;
    ss >> res;
    if (ss.fail() or not ss.eof())
        throw string("bad argument: ") + s;
    return res;
}

template<typename ValueT>
void check_number(string val_name, ValueT val, ValueT from,
                  ValueT to=numeric_limits<ValueT>::max())
{
    if (val < from)
        throw val_name + string(" is too small");
    if (val > to)
        throw val_name + string(" is too big");
}

void check_argc(int argc, int from, int to=numeric_limits<int>::max())
{
    if (argc < from)
        throw string("too few arguments for operation");

    if (argc > to)
        throw string("too many arguments for operation");
}

Matrix<double> parse_kernel(const std::string& kernel)
{
    std::vector<std::vector<double> > matrix;
    std::stringstream ss_kernel(kernel);
    std::string srow;
    while (std::getline(ss_kernel, srow, ';')) {
	std::stringstream ss_row(srow);
	std::vector<double> row;
	double number;
	bool wantMore = true;
        while (wantMore && ss_row >> std::noskipws >> number) {
	    row.push_back(number);
	    if (ss_row.peek() == ',') {
		ss_row.ignore();
		wantMore = true;
	    } else if (ss_row.peek() != EOF) {
		throw std::string("invalid kernel: invalid delimeter");
	    } else {
		wantMore = false;
	    }
	}
	if (ss_row.fail() && wantMore) {
	    throw std::string("invalid kernel: invalid character");
	}
	matrix.push_back(row);
    }

    int nrows = matrix.size();
    int ncols = matrix[0].size();
    
    /* Check that every matrix row has same number of elements */
    for (int i = 0; i < nrows; ++i) {
	if (int(matrix[i].size()) != ncols) {
	    throw std::string("invalid kernel: rows aren't of same size");
	}
    }

    if (nrows % 2 != 1 || ncols % 2 != 1) {
	throw std::string("invalid kernel: matrix dimensions must be odd numbers");
    }

    Matrix<double> retKernel(nrows, ncols);
    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    retKernel(i, j) = matrix[i][j];
	}
    }
    return retKernel;
}

int main(int argc, char **argv)
{
    try {
        check_argc(argc, 2);
        if (string(argv[1]) == "--help") {
            print_help(argv[0]);
            return 0;
        }

        check_argc(argc, 4);
        Image src_image = load_image(argv[1]), dst_image;

        string action(argv[3]);

        if (action == "--sobel-x") {
            check_argc(argc, 4, 4);
            dst_image = sobel_x(src_image);
        } else if (action == "--sobel-y") {
            check_argc(argc, 4, 4);
            dst_image = sobel_y(src_image);
        } else if (action == "--unsharp") {
            check_argc(argc, 4, 4);
            dst_image = unsharp(src_image);
        } else if (action == "--gray-world") {
            check_argc(argc, 4, 4);
            dst_image = gray_world(src_image);
        } else if (action == "--resize") {
            check_argc(argc, 5, 5);
            double scale = read_value<double>(argv[4]);
            dst_image = resize(src_image, scale);
        }  else if (action == "--custom") {
            check_argc(argc, 5, 5);
            Matrix<double> kernel = parse_kernel(argv[4]);
            // Function custom is useful for making concrete linear filtrations
            // like gaussian or sobel. So, we assume that you implement custom
            // and then implement concrete filtrations using this function.
            // For example, sobel_x may look like this:
            // sobel_x (...) {
            //    Matrix<double> kernel = {{-1, 0, 1},
            //                             {-2, 0, 2},
            //                             {-1, 0, 1}};
            //    return custom(src_image, kernel);
            // }
	    dst_image = custom(src_image, kernel);
        } else if (action == "--autocontrast") {
            check_argc(argc, 4, 5);
            double fraction = 0.0;
            if (argc == 5) {
                fraction = read_value<double>(argv[4]);
                check_number("fraction", fraction, 0.0, 0.4);
            }
            dst_image = autocontrast(src_image, fraction);
        } else if (action == "--gaussian" || action == "--gaussian-separable") {
            check_argc(argc, 5, 6);
            double sigma = read_value<double>(argv[4]);
            check_number("sigma", sigma, 0.1, 100.0);
            int radius = 3 * sigma;
            if (argc == 6) {
                radius = read_value<int>(argv[5]);
                check_number("radius", radius, 1);
            }
            if (action == "--gaussian") {
		dst_image = gaussian(src_image, sigma, radius);
            } else {
                dst_image = gaussian_separable(src_image, sigma, radius);
            }
        } else if (action == "--canny") {
            check_argc(argc, 6, 6);
            int threshold1 = read_value<int>(argv[4]);
            check_number("threshold1", threshold1, 0, 360);
            int threshold2 = read_value<int>(argv[5]);
            check_number("threshold2", threshold2, 0, 360);
            if (threshold1 >= threshold2)
                throw string("threshold1 must be less than threshold2");
            dst_image = canny(src_image, threshold1, threshold2);
        } else if (action == "--align") {
            check_argc(argc, 4, 7);
            if (argc >= 5) {
                string bicubicInterp(argv[4]);
		if (bicubicInterp == "--bicubic-interp") {
		    if (argc >= 6) {
			string postprocessing(argv[5]);
			if (postprocessing == "--gray-world" ||
			    postprocessing == "--unsharp") {
			    check_argc(argc, 6, 6);
			    dst_image = align(src_image, postprocessing, 0, true);
			} else if (postprocessing == "--autocontrast") {
			    double fraction = 0.0;
			    if (argc == 7) {
				fraction = read_value<double>(argv[6]);
				check_number("fraction", fraction, 0.0, 0.4);
			    }
			    dst_image = align(src_image, postprocessing, fraction, true);
			} else {
			    throw string("unknown align option ") + postprocessing;
			}
		    } else {
			dst_image = align(src_image, "", 0, true);
		    }
		} else {
		    string postprocessing(argv[4]);
		    if (postprocessing == "--gray-world" ||
			postprocessing == "--unsharp") {
			check_argc(argc, 5, 5);
			dst_image = align(src_image, postprocessing);
		    } else if (postprocessing == "--autocontrast") {
			double fraction = 0.0;
                    if (argc == 6) {
                        fraction = read_value<double>(argv[5]);
                        check_number("fraction", fraction, 0.0, 0.4);
                    }
                    dst_image = align(src_image, postprocessing, fraction);
		    } else {
			throw string("unknown align option ") + postprocessing;
		    }
		}
            } else {
		dst_image = align(src_image);
            }
        } else {
            throw string("unknown action ") + action;
        }

	for (uint i = 0; i < dst_image.n_rows; ++i) {
	    for (uint j = 0; j < dst_image.n_cols; ++j) {
		int r, g, b;
		std::tie(r, g, b) = dst_image(i, j);
		r = std::max(0, std::min(r, 255));
		g = std::max(0, std::min(g, 255));
		b = std::max(0, std::min(b, 255));
		dst_image(i, j) = std::make_tuple(r, g, b);
	    }
	}

        save_image(dst_image, argv[2]);
    } catch (const string &s) {
        cerr << "Error: " << s << endl;
        cerr << "For help type: " << endl << argv[0] << " --help" << endl;
        return 1;
    }
}
