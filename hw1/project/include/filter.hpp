#include <queue>
#include <vector>

#define sqr(x) ((x) * (x))

template<typename ValueT>
std::tuple<Matrix<ValueT>, Matrix<ValueT>, Matrix<ValueT> >
getChannels(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& img)
{
    int nrows = img.n_rows, ncols = img.n_cols;
    Matrix<ValueT> rChannel(nrows, ncols), gChannel(nrows, ncols), bChannel(nrows, ncols);
    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    ValueT r, g, b;
	    std::tie(r, g, b) = img(i, j);
	    rChannel(i, j) = r;
	    gChannel(i, j) = g;
	    bChannel(i, j) = b;
	}
    }
    return std::make_tuple(rChannel, gChannel, bChannel);
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
imgFromChannels(const Matrix<ValueT>& rChannel, const Matrix<ValueT>& gChannel, const Matrix<ValueT>& bChannel)
{
    int nrows = rChannel.n_rows, ncols = rChannel.n_cols;
    Matrix<std::tuple<ValueT, ValueT, ValueT> > dst(nrows, ncols);
    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    dst(i, j) = std::make_tuple(rChannel(i, j), gChannel(i, j), bChannel(i, j));
	}
    }
    return dst;
}

Matrix<double> gaussianKernel(double sigma, int radius)
{
    int size = 2 * radius + 1;
    Matrix<double> kernel(size, size);

    for (int i = 0; i < size; ++i) {
	for (int j = 0; j < size; ++j) {
	    kernel(i, j) = 1 / (2 * M_PI * sqr(sigma)) * exp(-(sqr(i - radius) + sqr(j - radius)) / (2 * sqr(sigma)));
	}
    }

    /* Normalize kernel */
    double sum = 0;
    for (int i = 0; i < size; ++i) {
	for (int j = 0; j < size; ++j) {
	    sum += kernel(i, j);
	}
    }
    for (int i = 0; i < size; ++i) {
	for (int j = 0; j < size; ++j) {
	    kernel(i, j) /= sum;
	}
    }
    return kernel;
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
customFast(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, const Matrix<double>& kernel)
{
    class CustomFilterOp {
    public:
	CustomFilterOp(const Matrix<double>& _kernel)
	    : radius_i(_kernel.n_rows / 2), radius_j(_kernel.n_cols / 2), kernel(_kernel)
	{}
	std::tuple<ValueT, ValueT, ValueT> operator()(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& m) const
	{
	    double size_i = 2 * radius_i + 1;
	    double size_j = 2 * radius_j + 1;
	    double sum_r = 0.0, sum_g = 0.0, sum_b = 0.0;
	    for (int i = 0; i < size_i; ++i) {
		for (int j = 0; j < size_j; ++j) {
		    ValueT r, g, b;
		    std::tie(r, g, b) = m(i, j);
		    sum_r += r * kernel(i, j);
		    sum_g += g * kernel(i, j);
		    sum_b += b * kernel(i, j);
		}
	    }
	
	    return std::make_tuple(sum_r, sum_g, sum_b);
	}

	int radius_i;
	int radius_j;
    private:
	Matrix<double> kernel;
    };

    return src.unary_map(CustomFilterOp(kernel));
}

template<typename ValueT>
Matrix<ValueT> gaussian1(const Matrix<ValueT>& src, double sigma, int radius)
{
    return custom1(src, gaussianKernel(sigma, radius));
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
gaussian(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, double sigma, int radius)
{
    Matrix<double> kernel = gaussianKernel(sigma, radius);
    return customFast(src, kernel);
}

Matrix<double> gaussianKernelI(double sigma, int radius)
{
    int size = 2 * radius + 1;
    Matrix<double> kernel_i(1, size);

    for (int i = 0; i < size; ++i) {
	kernel_i(0, i) = 1 / (sqrt(2 * M_PI) * sigma) * exp(-sqr(i - radius) / (2 * sqr(sigma)));
    }
    /* Normalize kernel */
    double sum = 0;
    for (int i = 0; i < size; ++i) {
	sum += kernel_i(0, i);
    }
    for (int i = 0; i < size; ++i) {
	kernel_i(0, i) /= sum;
    }
    return kernel_i;
}

template<typename ValueT>
Matrix<ValueT> gaussian_separable1(const Matrix<ValueT>& src, double sigma, int radius)
{
    Matrix<double> kernel_i = gaussianKernelI(sigma, radius);
    Matrix<double> kernel_j = kernel_i.transpose();
    return custom1(custom1(src, kernel_i), kernel_j);
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
gaussian_separable(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, double sigma, int radius)
{
    Matrix<double> kernel_i = gaussianKernelI(sigma, radius);
    Matrix<double> kernel_j = kernel_i.transpose();
    return customFast(customFast(src, kernel_i), kernel_j);
}

template<typename ValueT>
Matrix<ValueT> sobel1_x(const Matrix<ValueT>& src)
{
    Matrix<double> kernel = {{-1, 0, 1},
			     {-2, 0, 2},
			     {-1, 0, 1}};
    return custom1(src, kernel);
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
sobel_x(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src)
{
    Matrix<ValueT> rChannel, gChannel, bChannel;
    std::tie(rChannel, gChannel, bChannel) = getChannels(src);
    return imgFromChannels(sobel1_x(rChannel), sobel1_x(gChannel), sobel1_x(bChannel));
}

template<typename ValueT>
Matrix<ValueT> sobel1_y(const Matrix<ValueT>& src)
{
    Matrix<double> kernel = {{1, 2, 1},
			     {0, 0, 0},
			     {-1, -2, -1}};
    return custom1(src, kernel);
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
sobel_y(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src)
{
    Matrix<ValueT> rChannel, gChannel, bChannel;
    std::tie(rChannel, gChannel, bChannel) = getChannels(src);
    return imgFromChannels(sobel1_y(rChannel), sobel1_y(gChannel), sobel1_y(bChannel));
}

template<typename ValueT>
Matrix<ValueT> unsharp1(const Matrix<ValueT>& src)
{
    Matrix<double> kernel = {{-1.0 / 6, -2.0 / 3, -1.0 / 6},
			     {-2.0 / 3, 13.0 / 3, -2.0 / 3},
			     {-1.0 / 6, -2.0 / 3, -1.0 / 6}};
    return custom1(src, kernel);
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
unsharp(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src)
{
    Matrix<ValueT> rChannel, gChannel, bChannel;
    std::tie(rChannel, gChannel, bChannel) = getChannels(src);
    return imgFromChannels(unsharp1(rChannel), unsharp1(gChannel), unsharp1(bChannel));
}

template<typename ValueT>
Matrix<ValueT> custom1(const Matrix<ValueT>& src, const Matrix<double>& kernel)
{
    class CustomFilterOp {
    public:
	CustomFilterOp(const Matrix<double>& _kernel)
	    : radius_i(_kernel.n_rows / 2), radius_j(_kernel.n_cols / 2), kernel(_kernel)
	{}
	ValueT operator()(const Matrix<ValueT>& m) const
	{
	    double size_i = 2 * radius_i + 1;
	    double size_j = 2 * radius_j + 1;
	    double sum = 0.0;
	    for (int i = 0; i < size_i; ++i) {
		for (int j = 0; j < size_j; ++j) {
		    sum += m(i, j) * kernel(i, j);
		}
	    }
	
	    return sum;
	}

	int radius_i;
	int radius_j;
    private:
	Matrix<double> kernel;
    };

    return src.unary_map(CustomFilterOp(kernel));
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
custom(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, const Matrix<double>& kernel)
{
    Matrix<ValueT> rChannel, gChannel, bChannel;
    std::tie(rChannel, gChannel, bChannel) = getChannels(src);
    return imgFromChannels(custom1(rChannel, kernel), custom1(gChannel, kernel), custom1(bChannel, kernel));
}

template<typename ValueT>
Matrix<ValueT> gray_world(const Matrix<ValueT>& src)
{
    double r, g, b;
    double s_r = 0.0, s_g = 0.0, s_b = 0.0;
    for (uint i = 0; i < src.n_rows; ++i) {
	for (uint j = 0; j < src.n_cols; ++j) {
	    std::tie(r, g, b) = src(i, j);
	    s_r += r;
	    s_g += g;
	    s_b += b;
	}
    }

    double size = src.n_rows * src.n_cols;
    s_r /= size;
    s_g /= size;
    s_b /= size;
    double s = (s_r + s_g + s_b) / 3;

    double k_r = 1.0, k_g = 1.0, k_b = 1.0;
    /* to prevent division by zero later on */
    if (abs(s_r) > 1e-12) {
	k_r = s / s_r;
    }
    if (abs(s_g) > 1e-12) {
	k_g = s / s_g;
    }
    if (abs(s_b) > 1e-12) {
	k_b = s / s_b;
    }

    Matrix<ValueT> dst(src);
    for (uint i = 0; i < src.n_rows; ++i) {
	for (uint j = 0; j < src.n_cols; ++j) {
	    std::tie(r, g, b) = src(i, j);
	    r *= k_r;
	    g *= k_g;
	    b *= k_b;
	    dst(i, j) = std::make_tuple(r, g, b);
	}
    }
    
    return dst;
}

std::pair<int, int> get_dir(double theta)
{
    if ((7 * M_PI / 8 <= theta && theta <= M_PI) || (-M_PI <= theta && theta <= -7 * M_PI / 8)) {
	return std::make_pair(0, -1);
    }
    if (-7 * M_PI / 8 <= theta && theta <= -5 * M_PI / 8) {
	return std::make_pair(1, -1);
    }
    if (-5 * M_PI / 8 <= theta && theta <= -3 * M_PI / 8) {
	return std::make_pair(1, 0);
    }
    if (-3 * M_PI / 8 <= theta && theta <= -M_PI / 8) {
	return std::make_pair(1, 1);
    }
    if (-M_PI / 8 <= theta && theta <= M_PI / 8) {
	return std::make_pair(0, 1);
    }
    if (M_PI / 8 <= theta && theta <= 3 * M_PI / 8) {
	return std::make_pair(-1, 1);
    }
    if (3 * M_PI / 8 <= theta && theta <= 5 * M_PI / 8) {
	return std::make_pair(-1, 0);
    }
    if (5 * M_PI / 8 <= theta && theta <= 7 * M_PI / 8) {
	return std::make_pair(-1, -1);
    }
    //throw std::string("get_dir:theta isn't in range [-pi, pi]");
    return std::make_pair(0, 1);
}

enum PixelType {PSUPRESSED, PWEAK, PSTRONG};

template<typename ValueT>
int findCutLine(const Matrix<ValueT>& map, bool max)
{
    /* map(i, j) == 255 if (i, j) is border pixel and map(i, j) == 0 otherwise */

    int nrows = map.n_rows, ncols = map.n_cols;

    /* find first cut line */
    int cntMax = -1, cnt, iMax1 = -1, iMax2 = -1;
    for (int i = 0; i < nrows; ++i) {
	cnt = 0;
	for (int j = 0; j < ncols; ++j) {
	    if (map(i, j) == 255) {
		++cnt;
	    }
	}
	if (cnt > cntMax) {
	    cntMax = cnt;
	    iMax1 = i;
	}
    }

    /* find second cut line */
    cntMax = -1;
    for (int i = 0; i < nrows; ++i) {
	if (i == iMax1 || i == iMax1 - 1 || i == iMax1 + 1) {
	    /* skip neighbors of iMax1 */
	    continue;
	}
	cnt = 0;
	for (int j = 0; j < ncols; ++j) {
	    if (map(i, j) == 255) {
		++cnt;
	    }
	}
	if (cnt > cntMax) {
	    cntMax = cnt;
	    iMax2 = i;
	}
    }

    if (max) {
	return std::max(iMax1, iMax2);
    }
    return std::min(iMax1, iMax2);
}

template<typename ValueT>
Matrix<ValueT> canny1(const Matrix<ValueT>& src, int threshold1, int threshold2)
{
    int nrows = src.n_rows, ncols = src.n_cols;
    Matrix<double> img(nrows, ncols), i_x, i_y, grad(nrows, ncols), theta(nrows, ncols);

    /* Copy src to img */
    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    img(i, j) = src(i, j);
	}
    }

    /* ======= step 1 ====== */
    img = gaussian1(img, 1.4, 2);

    /* ======= step 2 ====== */
    i_x = sobel1_x(img);
    i_y = sobel1_y(img);

    for (int i = 0; i < nrows; ++i) {
    	for (int j = 0; j < ncols; ++j) {
    	    grad(i, j) = sqrt(sqr(i_x(i, j)) + sqr(i_y(i, j)));
    	    theta(i, j) = atan2(i_y(i, j), i_x(i, j));
    	}
    }

    /* ======= step 3 ======== */
    Matrix<PixelType> map(nrows, ncols);

    int idir, jdir, i1, j1, i2, j2;
    for (int i = 0; i < nrows; ++i) {
    	for (int j = 0; j < ncols; ++j) {
	    map(i, j) = PSTRONG;

    	    std::tie(idir, jdir) = get_dir(theta(i, j));
    	    i1 = i + idir; j1 = j + jdir;
    	    i2 = i - idir; j2 = j - jdir;
    	    if (i1 >= 0 && i1 < nrows && j1 >= 0 && j1 < ncols) {
    	        if (i2 >= 0 && i2 < nrows && j2 >= 0 && j2 < ncols) {
    		    if (!(grad(i, j) > grad(i1, j1) && grad(i, j) > grad(i2, j2))) {
    			map(i, j) = PSUPRESSED;
    		    }
    		}
    	    }
    	}
    }

    /* ========= step 4 ========== */
    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    if (map(i, j) != PSUPRESSED) { /* exclude pixels suppressed on step 3 */
		if (grad(i, j) < threshold1) {
		    map(i, j) = PSUPRESSED;
		} else if (grad(i, j) > threshold2) {
		    map(i, j) = PSTRONG;
		} else {
		    map(i, j) = PWEAK;
		}
	    }
	}
    }

    /* because our filters (gaussian, sobel) ignore edges of image
     we make sure that those edges won't be included into border map */
    const int NUM_IGNORED_EDGE_PIXELS = 2;
    for (int i = nrows - NUM_IGNORED_EDGE_PIXELS + 1; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    map(i, j) = PSUPRESSED;
	}
    }
    for (int i = 0; i < NUM_IGNORED_EDGE_PIXELS; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    map(i, j) = PSUPRESSED;
	}
    }
    for (int j = ncols - NUM_IGNORED_EDGE_PIXELS + 1; j < ncols; ++j) {
	for (int i = 0; i < nrows; ++i) {
	    map(i, j) = PSUPRESSED;
	}
    }
    for (int j = 0; j < NUM_IGNORED_EDGE_PIXELS; ++j) {
	for (int i = 0; i < nrows; ++i) {
	    map(i, j) = PSUPRESSED;
	}
    }

    /* =========== step 5 ============== */
    std::vector<std::pair<int, int> > strong;
    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    if (map(i, j) == PSTRONG) {
		strong.push_back(std::make_pair(i, j));
	    }
	}
    }

    const int di[] = {-1, -1, -1, 0, 0, 1, 1, 1};
    const int dj[] = {-1, 0, 1, -1, 1, -1, 0, 1};
    
    std::queue<std::pair<int, int> > queue;
    for (auto p0 : strong) {
	for (queue.push(p0); !queue.empty(); queue.pop()) {
	    std::pair<int, int> p = queue.front();
	    int i, j;
	    for (int k = 0; k < 8; k++) {
		i = p.first + di[k];
		j = p.second + dj[k];
		if (i >= 0 && i < nrows && j >= 0 && j < ncols) {
		    if (map(i, j) == PWEAK) {
			map(i, j) = PSTRONG;
			queue.push(std::make_pair(i, j));
		    }
		}
	    }
	}
    }
    
    /* from here map(i, j) == PSTRONG if and only if (i, j) is border pixel */

    Matrix<ValueT> dst(nrows, ncols);
    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    dst(i, j) = (map(i, j) == PSTRONG) ? 255 : 0;
	}
    }
    return dst;
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
canny(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, int threshold1, int threshold2)
{
    Matrix<ValueT> rChannel, gChannel, bChannel;
    std::tie(rChannel, gChannel, bChannel) = getChannels(src);
    rChannel = canny1(rChannel, threshold1, threshold2);
    return imgFromChannels(rChannel, rChannel, rChannel);
}

template<typename ValueT>
Matrix<ValueT> cropImage1(const Matrix<ValueT>& src, int threshold1, int threshold2)
{
    int nrows = src.n_rows, ncols = src.n_cols;

    Matrix<ValueT> map = canny1(src, threshold1, threshold2);
    const double FRAC = 0.10;
    int itop = findCutLine(map.submatrix(0, 0, FRAC * nrows, ncols), true);
    int jleft = findCutLine(map.submatrix(0, 0, nrows, FRAC * ncols).transpose(), true);
    int jright = findCutLine(map.submatrix(0, ncols - int(FRAC * ncols), nrows, FRAC * ncols).transpose(), false) + (ncols - int(FRAC * ncols));
    int ibottom = findCutLine(map.submatrix(nrows - int(FRAC * nrows), 0, FRAC * nrows, ncols), false) + (nrows - int(FRAC * nrows));
    
    int nrowsNew = ibottom - itop - 1;
    int ncolsNew = jright - jleft - 1;
    Matrix<ValueT> dst(nrowsNew, ncolsNew);
    for (int i = 0; i < nrowsNew; ++i) {
    	for (int j = 0; j < ncolsNew; ++j) {
    	    dst(i, j) = src(i + itop + 1, j + jleft + 1);
    	}
    }

    return dst;
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
cropImage(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, int threshold1, int threshold2)
{
    Matrix<ValueT> rChannel, gChannel, bChannel;
    std::tie(rChannel, gChannel, bChannel) = getChannels(src);
    rChannel = cropImage1(rChannel, threshold1, threshold2);
    return imgFromChannels(rChannel, rChannel, rChannel);
}

template<typename ValueT>
ValueT mse(const Matrix<ValueT>& x, const Matrix<ValueT>& y)
{
    if (x.n_rows != y.n_rows || x.n_cols != y.n_cols) {
	throw "mse: x and y must be of same size";
    }

    int nrows = x.n_rows, ncols = x.n_cols;
    double sum = 0;
    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    sum += sqr(x(i, j) - y(i, j));
	}
    }
    return sum / (nrows * ncols);
}

template<typename ValueT>
ValueT crossCorr(const Matrix<ValueT>& x, const Matrix<ValueT>& y)
{
    if (x.n_rows != y.n_rows || x.n_cols != y.n_cols) {
	throw "crossCorr: x and y must be of same size";
    }

    int nrows = x.n_rows, ncols = x.n_cols;
    double sum = 0;
    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    sum += x(i, j) * y(i, j);
	}
    }
    return sum;
}

template<typename ValueT>
std::pair<int, int> bestShift(const Matrix<ValueT>& x, const Matrix<ValueT>& y)
{
    const int MAX_SHIFT = 15;

    int l, r, t, b;
    double error, errorBest = 1e10;
    int slBest = 0, stBest = 0;
    for (int sl = -MAX_SHIFT; sl <= MAX_SHIFT; ++sl) {
	for (int st = -MAX_SHIFT; st <= MAX_SHIFT; ++st) {
	    l = std::max(0, sl);
	    t = std::max(0, st);
	    r = std::min<int>(x.n_cols - 1, sl + y.n_cols - 1);
	    b = std::min<int>(x.n_rows - 1, st + y.n_rows - 1);
	    
	    error = mse(x.submatrix(t, l, b - t + 1, r - l + 1),
			y.submatrix(t - st, l - sl, b - t + 1, r - l + 1));
	    if (error < errorBest) {
		errorBest = error;
		slBest = sl;
		stBest = st;
	    }
	}
    }

    return std::make_pair(slBest, stBest);
}

Image merge(const Image& img, const Image& yImg, int channel, std::pair<int, int> bShift)
{
    Matrix<int> x, y;
    if (channel == 0) {
	x = std::get<0>(getChannels(img));
        y = std::get<0>(getChannels(yImg));
    } else if (channel == 1) {
	x = std::get<1>(getChannels(img));
        y = std::get<1>(getChannels(yImg));
    } else {
	x = std::get<2>(getChannels(img));
        y = std::get<2>(getChannels(yImg));
    }

    int slBest, stBest, l, r, b, t;
    slBest = bShift.first, stBest = bShift.second;
    
    l = std::max(0, slBest);
    t = std::max(0, stBest);
    r = std::min<int>(x.n_cols - 1, slBest + y.n_cols - 1);
    b = std::min<int>(x.n_rows - 1, stBest + y.n_rows - 1);

    int resRows = b - t + 1, resCols = r - l + 1;
    Image dst = img.submatrix(t, l, resRows, resCols);
    Matrix<int> yPart = y.submatrix(t - stBest, l - slBest, resRows, resCols);
    for (int i = 0; i < resRows; ++i) {
	for (int j = 0; j < resCols; ++j) {
	    int rc, gc, bc;
	    std::tie(rc, gc, bc) = dst(i, j);
	    if (channel == 0) {
		rc = yPart(i, j);
	    } else if (channel == 1) {
		gc = yPart(i, j);
	    } else {
		bc = yPart(i, j);
	    }
	    dst(i, j) = std::make_tuple(rc, gc, bc);
	}
    }
    
    return dst;
}

Image align(const Image& src, const string& postprocessing, double fraction, bool doBicubic)
{
    const double COEF = 2.0;
    const int THRESHOLD = 200;

    int nrows = src.n_rows, ncols = src.n_cols;
    Image b = src.submatrix(0, 0, nrows / 3, ncols);
    Image g = src.submatrix(nrows / 3, 0, nrows / 3, ncols);
    Image r = src.submatrix(2 * nrows / 3, 0, nrows / 3, ncols);

    const int THRESHOLD1 = 30;
    const int THRESHOLD2 = 230;
    r = cropImage(r, THRESHOLD1, THRESHOLD2);
    g = cropImage(g, THRESHOLD1, THRESHOLD2);
    b = cropImage(b, THRESHOLD1, THRESHOLD2);

    Image rSmall = r, gSmall = g, bSmall = b;
    int cnt = 0;
    while (std::min<int>(rSmall.n_rows, rSmall.n_cols) > THRESHOLD) {
	rSmall = resize(rSmall, 1.0 / COEF, doBicubic);
	gSmall = resize(gSmall, 1.0 / COEF, doBicubic);
	bSmall = resize(bSmall, 1.0 / COEF, doBicubic);
	++cnt;
    }

    std::pair<int, int> rgShift = bestShift(std::get<1>(getChannels(rSmall)),
					    std::get<1>(getChannels(gSmall)));
    std::pair<int, int> rbShift = bestShift(std::get<2>(getChannels(rSmall)),
					    std::get<2>(getChannels(bSmall)));
    for (int k = 0; k < cnt; ++k) {
    	rgShift.first *= COEF;
    	rgShift.second *= COEF;

    	rbShift.first *= COEF;
    	rbShift.second *= COEF;
    }

    Image dst = r;
    dst = merge(dst, g, 1, rgShift);
    dst = merge(dst, b, 2, rbShift);

    /* ====== postprocessing ====== */
    if (postprocessing == "--gray-world") {
    	dst = gray_world(dst);
    } else if (postprocessing == "--unsharp") {
    	dst = unsharp(dst);
    } else if (postprocessing == "--autocontrast") {
    	dst = autocontrast(dst, fraction);
    }

    return dst;
}

int linear(int p0, int p1, double x)
{
    return (p1 - p0) * x + p0;
}

template<typename ValueT>
Matrix<ValueT> resizeBilinear1(const Matrix<ValueT>& src, double scale)
{
    //    std::cerr << "bilinear interpolation\n";

    int nrowsOld = src.n_rows, ncolsOld = src.n_cols;
    int nrowsNew = ceil(nrowsOld * scale), ncolsNew = ceil(ncolsOld * scale);

    Matrix<ValueT> dst(nrowsNew, ncolsNew);

    int i1, i2, j1, j2;
    int f11, f12, f22, f21;
    double i, j, di, dj;
    for (int iNew = 0; iNew < nrowsNew; ++iNew) {
	for (int jNew = 0; jNew < ncolsNew; ++jNew) {
	    i = iNew / scale, j = jNew / scale;
	    i1 = floor(i), j1 = floor(j);
	    di = i - i1, dj = j - j1;

	    i2 = i1 + 1, j2 = j1 + 1;

	    f11 = src.get(i1, j1);
	    f12 = src.get(i1, j2);

	    f21 = src.get(i2, j1);
	    f22 = src.get(i2, j2);

	    dst(iNew, jNew) = linear(linear(f11, f12, dj), linear(f21, f22, dj), di);
	}
    }

    return dst;
}

double cubic(int p0, int p1, int p2, int p3, double x)
{
    return (-0.5 * p0 + 1.5 * p1 - 1.5 * p2 + 0.5 * p3) * (x * x * x) +
	(p0 - 2.5 * p1 + 2 * p2 - 0.5 * p3) * (x * x) +
	(-0.5 * p0 + 0.5 * p2) * x +
	p1;
}

template<typename ValueT>
Matrix<ValueT> resizeBicubic1(const Matrix<ValueT>& src, double scale)
{
    //    std::cerr << "bicubic interpolation\n";

    int nrowsOld = src.n_rows, ncolsOld = src.n_cols;
    int nrowsNew = ceil(nrowsOld * scale), ncolsNew = ceil(ncolsOld * scale);

    Matrix<ValueT> dst(nrowsNew, ncolsNew);
    double i, j, di, dj;
    int i1, i2, i3, i4,
	j1, j2, j3, j4;
    int f11, f12, f13, f14,
	f21, f22, f23, f24,
	f31, f32, f33, f34,
	f41, f42, f43, f44;
    for (int iNew = 0; iNew < nrowsNew; ++iNew) {
	for (int jNew = 0; jNew < ncolsNew; ++jNew) {
	    i = iNew / scale, j = jNew / scale;
	    i2 = floor(i), j2 = floor(j);
	    di = i - i2, dj = j - j2;

	    i1 = i2 - 1, j1 = j2 - 1;
	    i3 = i2 + 1, j3 = j2 + 1;
	    i4 = i2 + 2, j4 = j2 + 2;
	    
	    f11 = src.get(i1, j1);
	    f12 = src.get(i1, j2);
	    f13 = src.get(i1, j3);
	    f14 = src.get(i1, j4);

	    f21 = src.get(i2, j1);
	    f22 = src.get(i2, j2);
	    f23 = src.get(i2, j3);
	    f24 = src.get(i2, j4);

	    f31 = src.get(i3, j1);
	    f32 = src.get(i3, j2);
	    f33 = src.get(i3, j3);
	    f34 = src.get(i3, j4);

	    f41 = src.get(i4, j1);
	    f42 = src.get(i4, j2);
	    f43 = src.get(i4, j3);
	    f44 = src.get(i4, j4);

	    dst(iNew, jNew) = cubic(cubic(f11, f12, f13, f14, dj),
				    cubic(f21, f22, f23, f24, dj),
				    cubic(f31, f32, f33, f34, dj),
				    cubic(f41, f42, f43, f44, dj),
				    di);
	}
    }

    return dst;
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
resizeBilinear(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, double scale)
{
    Matrix<ValueT> rChannel, gChannel, bChannel;
    std::tie(rChannel, gChannel, bChannel) = getChannels(src);
    return imgFromChannels(resizeBilinear1(rChannel, scale), resizeBilinear1(gChannel, scale), resizeBilinear1(bChannel, scale));
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
resizeBicubic(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, double scale)
{
    Matrix<ValueT> rChannel, gChannel, bChannel;
    std::tie(rChannel, gChannel, bChannel) = getChannels(src);
    return imgFromChannels(resizeBicubic1(rChannel, scale), resizeBicubic1(gChannel, scale), resizeBicubic1(bChannel, scale));
}

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
resize(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, double scale, bool doBicubic)
{
    if (doBicubic) {
	return resizeBicubic(src, scale);
    }
    return resizeBilinear(src, scale);
}

Image autocontrast(const Image& src, double fraction)
{
    int nrows = src.n_rows, ncols = src.n_cols;
    int size = nrows * ncols;
    std::vector<double> brightness;

    int r, g, b;

    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    std::tie(r, g, b) = src(i, j);
	    double y = 0.2125 * r + 0.7154 * g + 0.0721 * b;
	    brightness.push_back(y);
	}
    }

    std::sort(brightness.begin(), brightness.end());

    int numCrop = size * fraction;
    double yMin = brightness[numCrop];
    double yMax = brightness[brightness.size() - numCrop - 1];

    /* to prevent division by zero later on */
    if (abs(yMax - yMin) < 1e-12) {
	return src;
    }

    Image dst(nrows, ncols);
    for (int i = 0; i < nrows; ++i) {
	for (int j = 0; j < ncols; ++j) {
	    std::tie(r, g, b) = src(i, j);
	    
	    r = round(255 * (r - yMin) / (yMax - yMin));
	    g = round(255 * (g - yMin) / (yMax - yMin));
	    b = round(255 * (b - yMin) / (yMax - yMin));
	    dst(i, j) = std::make_tuple(r, g, b);
	}
    }

    return dst;
}
