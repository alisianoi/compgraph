#pragma once

#include <tuple>

#include "matrix.h"

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
    gaussian(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, double sigma, int radius);

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
    gaussian_separable(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, double sigma, int radius);

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
    sobel_x(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src);

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
    sobel_y(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src);

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
    unsharp(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src);

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
    custom(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, const Matrix<double>& kernel);

template<typename ValueT>
Matrix<ValueT> gray_world(const Matrix<ValueT>& src);

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
    canny(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, int threshold1, int threshold2);

template<typename ValueT>
Matrix<std::tuple<ValueT, ValueT, ValueT> >
    resize(const Matrix<std::tuple<ValueT, ValueT, ValueT> >& src, double scale, bool doBicubic = false);

Image align(const Image& src, const string& postprocessing = "", double fraction = 0.0, bool doBicubic = false);
Image autocontrast(const Image& src, double fraction);

#include "filter.hpp"
