#pragma once

#include <stdlib.h>

#include <stdexcept>
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <cmath>

#include <cfloat>

// force static linkage
#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "IL/il.h"

#include "shader.h"

extern GLuint vShader;
extern GLuint fShader;
extern GLuint program;

enum PLANETS {SUN, MERCURY, VENUS, EARTH, MARS, JUPITER, SATURN,
              URANUS, NEPTUNE, PLUTO, nPLANETS};

enum VAO_IDS {GRID, ISOCOH, SPHERE, nVAOs};
extern GLuint VAO[];

enum VBO_IDS {GRID_VBO, GRID_COORDS_VBO,
              ISOCOH_VBO, ISOCOH_COORDS_VBO,
              SPHERE_VBO, SPHERE_COORDS_VBO, nVBOs};
extern GLuint VBO[];

extern GLuint TEX[];
extern ILuint IMGS[];

// camera is responsible for the `view` transformation matrix from the
// `projetion * view * model` stack; The actual view matrix gets
// computed with `view = glm::lookAt(camera_pos, camera_focus,
// stage_up)` based on the setting below;
extern glm::mat4 view;
extern glm::vec3 camera_pos;
extern glm::vec3 camera_focus;
const glm::vec3 stage_up = glm::vec3(0.0f, 1.0f, 0.0f);

extern glm::mat4 proj;
// Field Of View, angle in degrees
const auto FOV = 45.0f;
// Near clipping plane (Z buffer)
const auto zNear = 0.1f;
// Far clipping plane (Z buffer)
const auto zFar = 1000.0f;

// power of root that will be applied to lengths in the `model` mode;
const float ROOTPOW = 0.25f;
// constant which will divide the lengths in `to scale` mode;
const float DENOM = float(1e+6);
const auto sphere_triag_level = 5;

// number of grid elements
const auto nGrid = 100;
const auto totGridPoints =
    (2 * nGrid + 1) * (2 * nGrid + 1);
// whether to show grid
extern bool show_grid;
// whether to show topology
extern bool wireframe;
// which display mode to choose
extern bool schematic;
// whether to suspend animation
extern bool suspend_animation;

// physics parameters
const double G = 6.67384e-11;
extern glm::mat4 scale_incline[nPLANETS];
extern float current_rotation_angle[nPLANETS];
extern float rotate_per_delta_t[nPLANETS];

extern glm::vec3 coords[nPLANETS];
extern glm::vec3 speeds[nPLANETS];
extern const float masses[nPLANETS];

extern const glm::vec3 avg_radii[];

// solar system seconds per model frame
extern const float delta_t;
extern unsigned int iterations;

const std::string textures_path = "@SOLAR_TEXTURES_BIN@";
const std::string shader_path = "@SOLAR_SHADERS_BIN@";

// Mouse info
extern int mouse_button;
extern int mouse_state;
extern int mouse_x;
extern int mouse_y;

// window size
extern unsigned int windowHeight;
extern unsigned int windowWidth;

void init(void);
void init_textures(void);

void display(void);
void cleanup(void)
    throw(std::runtime_error);
void reshape(int width, int height);
void keyboard(unsigned char key, int x, int y);
void mouse_buttons(int button, int state, int x, int y);
void mouse_passive_motion(int x, int y);
