#pragma once

#include <string>
#include <memory>
#include <fstream>
#include <iostream>
#include <stdexcept>

#include <GL/glew.h>

class ShaderObject
{
    GLuint _shader;
    GLenum _shader_type;

    std::string _path;
    std::shared_ptr<char> _source_code;
    GLint _source_length;

    void createCopyCompile(void)
        throw(std::runtime_error);
public:
    ShaderObject();
	ShaderObject(const std::string& path, GLenum type)
        throw(std::runtime_error);

    ShaderObject(const ShaderObject& src)
        throw(std::runtime_error);

    ShaderObject(ShaderObject&& src) noexcept;

	~ShaderObject() noexcept;

    auto getName() const -> decltype(_shader) {return _shader;}

    // reassigning ShaderObject doesn't make sense, as well as moving;
    // but we need that because these should go to global scope;
    // Therefore, if an object gets reassigned or moved, make a zombie;
    ShaderObject& operator=(const ShaderObject& src) = delete;
    ShaderObject& operator=(const ShaderObject&& src) = delete;
};
