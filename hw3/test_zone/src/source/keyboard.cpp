#include "main.h"

bool suspend_animation = false;
bool show_grid = false;
bool wireframe = false;
bool schematic = true;

void
keyboard(unsigned char key, int x, int y)
{
    if (key == 'W') { // add a coordinate grid
        show_grid = not show_grid;
    } else if (key == 'S') {
        wireframe = not wireframe;
    } else if (key == 'M' or key == 'm') {
        schematic = not schematic;
    } else if (key == 'q') {
        if (iterations + 1 != UINT_MAX) {
            iterations += 1;
        } else {
            std::cerr << "WARNING: can't go faster, uint range out"
                      << std::endl;
        }
    } else if (key == 'Q') {
        if (iterations != 1) {
            iterations -= 1;
        }
    } else if (key == ' ') {
        suspend_animation = not suspend_animation;
    } else if (key == 'R') { // reset the camera and speed
        iterations = 1;
        camera_pos = glm::vec3(50.0f, 50.0f, 50.0f);
        camera_focus = glm::vec3(0.0f, 0.0f, 0.0f);

        view = glm::lookAt(camera_pos, camera_focus, stage_up);
    } else if (key == '1' or key == '2' or key == '3' or key == '4'
               or key == '5' or key == '6' or key == '7' or key == '8'
               or key == '9' or key == '0') {
        int p;
        if (key == '0') {
            p = 9; // actually, Pluto;
        } else {
            p = key - '0' - 1;
        }

        suspend_animation = true;
        auto direction = glm::vec3(0.0f);
        auto model_length = 0.0f;
        const auto relcoord = coords[p] - coords[SUN];
        if (relcoord != glm::vec3(0.0f)) {
            direction = glm::normalize(relcoord);
            // ROOTPOW is a global constant from main.h;
            if (schematic) {
                model_length = pow(glm::length(relcoord), ROOTPOW);
            } else {
                model_length = glm::length(relcoord) / DENOM;
            }
        }

        camera_pos = model_length * direction;
        if (camera_pos == glm::vec3(0.0f)) {
            camera_pos = glm::vec3(50.0f, 50.0f, 50.0f);
        }

        camera_focus = glm::vec3(0.0f, 0.0f, 0.0f);
        view = glm::lookAt(camera_pos, camera_focus, stage_up);
    } else if (key == 'w') {
        const auto camera_dir =
            glm::normalize(camera_focus - camera_pos);

        auto x = 1.0f;
        for (auto i = 0; i < nPLANETS; ++i) {
            const auto v = camera_pos - coords[i] + coords[SUN];
            const auto object_dir = glm::normalize(v);
            const auto object_dist = glm::length(v);

            if (glm::dot(camera_dir, object_dir) > 0.90f) {
                x = std::max(1.0f, float(pow(object_dist, 0.1)));
            }
        }

        camera_pos = camera_pos + x * camera_dir;
        camera_focus = camera_focus + x * camera_dir;

        view = glm::lookAt(camera_pos, camera_focus, stage_up);
    } else if (key == 's') {
        const auto camera_dir =
            glm::normalize(camera_focus - camera_pos);

        camera_pos = camera_pos - camera_dir;
        camera_focus = camera_focus - camera_dir;

        view = glm::lookAt(camera_pos, camera_focus, stage_up);
    } else if (key == 'd') {
        const auto direction =
            glm::normalize(camera_focus - camera_pos);
        const auto right_yaw = glm::cross(direction, stage_up);

        camera_pos = camera_pos + right_yaw;
        camera_focus = camera_focus + right_yaw;

        view = glm::lookAt(camera_pos, camera_focus, stage_up);
        glutPostRedisplay();
    } else if (key == 'a') {
        const auto direction =
            glm::normalize(camera_focus - camera_pos);
        const auto left_yaw = glm::cross(stage_up, direction);

        camera_pos = camera_pos + left_yaw;
        camera_focus = camera_focus + left_yaw;

        view = glm::lookAt(camera_pos, camera_focus, stage_up);
    } else {
        std::cerr << "key: " << key << " pressed" << std::endl;
    }

    glutPostRedisplay();
}
