#include "main.h"

int mouse_button;
int mouse_state;
int mouse_x;
int mouse_y;

void
mouse_buttons(int button, int state, int x, int y)
{
    switch (button) {
    case GLUT_LEFT_BUTTON:
        std::cerr << "left button pressed" << std::endl;
        break;
    case GLUT_RIGHT_BUTTON:
        std::cerr << "right button pressed" << std::endl;
        break;
    case GLUT_MIDDLE_BUTTON:
        std::cerr << "middle button pressed" << std::endl;
        break;
    default:
        std::cerr << "Some mouse button pressed" << std::endl;
        break;
    }
}

void
mouse_passive_motion(int x, int y)
{
    static bool should_warp = false;
    static const GLfloat speed = 0.1f;

    if (should_warp) {
        glm::vec2 mouse_pos = glm::vec2(x - mouse_x, y - mouse_y);

        glm::vec3 direction = camera_focus - camera_pos;
    
        glm::mat4 hor_rot(1.0f);
        hor_rot = glm::rotate(hor_rot, -speed * mouse_pos[0], stage_up);
        direction = glm::vec3(hor_rot * glm::vec4(direction, 0));

        glm::mat4 ver_rot(1.0f);
        const auto pivot = glm::cross(direction, stage_up);
        ver_rot = glm::rotate(ver_rot, -speed * mouse_pos[1], pivot);
        direction = glm::vec3(ver_rot * glm::vec4(direction, 0));

        camera_focus = camera_pos + direction;

        view = glm::lookAt(camera_pos, camera_focus, stage_up);

        glutWarpPointer(windowWidth / 2, windowHeight / 2);
        glutPostRedisplay();

        should_warp = false;
    } else {
        should_warp = true;
    }
}
