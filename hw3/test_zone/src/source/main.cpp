#include "main.h"

unsigned int windowHeight = 600;
unsigned int windowWidth = 800;

void animate_when_idle(void)
{
    if (not suspend_animation) {
        glutPostRedisplay();
    }
}

int
main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    
    glutInitContextVersion(3, 1);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitContextFlags(GLUT_DEBUG | GLUT_FORWARD_COMPATIBLE);

    glutInitWindowPosition(0, 0);
    glutInitWindowSize(windowHeight, windowWidth);

    glutCreateWindow("Allonz'y!");

    // following code line might help *find* some functionality when
    // running on beta drivers. Try it out when you think there is
    // some functionality missing. For me, there was no
    // glGenVertexArrays;
    glewExperimental = GL_TRUE;
    const auto status = glewInit();
    if (status != GLEW_OK) {
        std::cerr << "MAYDAY: glewInit() failed, crushlanding."
                  << std::endl;
        exit(EXIT_FAILURE);
    }

    auto gl_error = glGetError();
    const std::string prototype = __func__;
    const auto separator =
        "******************************************************";
    if (gl_error != GL_NO_ERROR) {
        const auto warning_header = prototype + " WARNING:";
        const auto warning_message =
            "There are unattended errors left in OpenGL context.";
        const auto warning_action =
            "Little can be done about that, let's try to continue.";
        std::cerr << separator << std::endl
                  << warning_header << std::endl
                  << warning_message << std::endl
                  << warning_action << std::endl
                  << separator << std::endl;
    } else {
        const auto message_header = prototype + " MESSAGE:";
        const auto message_content =
            "Error state of OpenGL context has been cleared";
        std::cerr << separator << std::endl
                  << message_header << std::endl
                  << message_content << std::endl;
    }

    const auto glVendor = glGetString(GL_VENDOR);
    const auto glRenderer = glGetString(GL_RENDERER);

    const auto glVersion = glGetString(GL_VERSION);
    const auto glslVersion =
        glGetString(GL_SHADING_LANGUAGE_VERSION);

    const auto glewVersion = glewGetString(GLEW_VERSION);

    std::cerr << "This is your captain speaking, welcome aboard:"
              << std::endl
              << "Please note that a value of 0 indicates failure"
              << std::endl << "=============================="
              << std::endl << "GL_VENDOR     " << glVendor
              << std::endl << "GL_RENDERER   " << glRenderer
              << std::endl
              << std::endl << "GL_VERSION    " << glVersion
              << std::endl << "GLSL_VERSION  " << glslVersion
              << std::endl
              << std::endl << "GLEW          " << glewVersion
              << std::endl << "=============================="
              << std::endl << "Thank you for your attention, "
              << "we are now ready to take off." << std::endl;
    if (glVendor == 0 or glRenderer == 0
        or glVersion == 0 or glslVersion == 0) {
        const auto errmsg = "There is an error in the table above";
        std::cerr << errmsg << std::endl;
        exit(EXIT_FAILURE);
    }

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);

    glutMouseFunc(mouse_buttons);
    glutKeyboardFunc(keyboard);

    glutPassiveMotionFunc(mouse_passive_motion);

    glutCloseFunc(cleanup);

    glutIdleFunc(animate_when_idle);

    init();

    glutMainLoop();

    // glutMainLoop is never supposed to return. Anyways.
    exit(EXIT_SUCCESS);
}
