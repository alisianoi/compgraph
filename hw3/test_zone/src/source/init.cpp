#include "main.h"

GLuint vShader;
GLuint fShader;
GLuint program;

GLuint VAO[nVAOs];
GLuint VBO[nVBOs];
GLuint TEX[nPLANETS];

const float delta_t = 10;
unsigned int iterations = 1;

glm::mat4 scale_incline[nPLANETS];
float rotate_per_delta_t[nPLANETS];
float current_rotation_angle[nPLANETS];

glm::vec3 coords[nPLANETS] = {
    // SUN
    glm::vec3(-3.587908596635106E+05,
              -1.263365847270218E+04,
              9.166368072918009E+04),
    // MERCURY
    glm::vec3(2.693705871595619E+07,
              -1.427017526993466E+06,
              3.981532574646779E+07),
    // VENUS
    glm::vec3(-7.516123504840386E+06,
              -6.361445678009944E+06,
              1.083995110582697E+08),
    // EARTH
    glm::vec3(9.214935380493926E+07,
              -1.560961036842257E+04,
              1.162398804913440E+08),
    // MARS
    glm::vec3(1.925541334868633E+08,
              7.784109759604024E+06,
              -1.528793878916806E+08),
    // JUPITER
    glm::vec3(7.618480179221750E+08,
              -2.483656477704518E+05,
              -1.308404872563110E+08),
    // SATURN
    glm::vec3(-1.022591702447227E+09,
              6.008502801056857E+07,
              -1.062988073620396E+09),
    // URANUS
    glm::vec3(5.529078700946414E+08,
              -3.611419689746372E+07,
              2.946083841697208E+09),
    // NEPTUNE
    glm::vec3(-1.954926579794473E+09,
              -5.276881058283679E+07,
              4.036568780165731E+09),
    // PLUTO
    glm::vec3(-4.775885697847751E+09,
              2.485565548180889E+08,
              9.074663629135412E+08),
};

const glm::vec3 avg_radii[nPLANETS] = {
    // SUN
    glm::vec3(695990),
    // MERCURY
    glm::vec3(2439.7),
    // VENUS
    glm::vec3(6051.8),
    // EARTH
    glm::vec3(6371),
    // MARS
    glm::vec3(3389.5),
    // JUPITER
    glm::vec3(69911),
    // SATURN
    glm::vec3(57316),
    // URANUS
    glm::vec3(25266),
    // NEPTUNE
    glm::vec3(24552.5),
    // PLUTO
    glm::vec3(1195),
};

const float masses[nPLANETS] = {
    // SUN
    1.99E+30,
    // MERCURY
    3.33E+23,
    // VENUS
    4.87E+24,
    // EARTH
    5.97E+24,
    // MARS
    6.42E+23,
    // JUPITER
    1.90E+27,
    // SATURN
    5.68E+26,
    // URANUS
    8.68E+25,
    // NEPTUNE
    1.02E+26,
    // PLUTO
    1.30E+22,
};

const float inclination[nPLANETS] = {
    // SUN
    7.25,
    // MERCURY
    2.11,
    // VENUS
    177.36,
    // EARTH
    23.44,
    // MARS
    25.1919,
    // JUPITER
    3.13,
    // SATURN
    26.73,
    // URANUS
    97.77,
    // NEPTUNE
    28.32,
    // PLUTO
    119.591
};

const float days2rotate[nPLANETS] = {
    // SUN
    25.38,
    // MERCURY
    58.646,
    // VENUS
    243.023,
    // EARTH
    0.99726968,
    // MARS
    1.029,
    // JUPITER
    0.4147,
    // SATURN
    0.4416,
    // URANUS
    0.71833,
    // NEPTUNE
    0.6653,
    // PLUTO
    6.38723
};

glm::vec3 speeds[nPLANETS] = {
    // SUN
    glm::vec3(2.825998901862247E-03,
              -2.451813399270692E-04,
              1.068407615399651E-02),
    // MERCURY
    glm::vec3(4.226944115449314E+01,
              6.860766793768278E+00,
              -3.712878513620788E+01),
    // VENUS
    glm::vec3(3.478693145745870E+01,
              3.521473884663605E-01,
              2.163430099495670E+00),
    // EARTH
    glm::vec3(2.319272201812972E+01,
              -9.394261929108316E-04,
              -1.903255921308336E+01),
    // MARS
    glm::vec3(-1.298811643007845E+01,
              1.711362049767525E-01,
              -1.805947412894641E+01),
    // JUPITER
    glm::vec3(-1.591321557884244E+00,
              2.983270239243695E-01,
              -1.303624071468951E+01),
    // SATURN
    glm::vec3(-6.985778300302598E+00,
              -1.241099702155237E-01,
              6.170956639472989E+00),
    // URANUS
    glm::vec3(6.375718119862134E+00,
              4.060658963891937E-02,
              -1.305921743797635E+00),
    // NEPTUNE
    glm::vec3(4.924012112924557E+00,
              -1.551671101516667E-01,
              2.332992134044233E+00),
    // PLUTO
    glm::vec3(-8.040967446408351E-02,
              -1.566356777898360E+00,
              5.444809029005323E+00),
};

glm::mat4 view;
glm::mat4 proj;

glm::vec3 camera_pos = glm::vec3(50.0f, 50.0f, 50.0f);
glm::vec3 camera_focus = glm::vec3(0.0f, 0.0f, 0.0f);

void generate_grid(void);
void generate_sphere(unsigned int level=0);
unsigned int
is_seam(const GLfloat& a, const GLfloat& b, const GLfloat& c);

void
init(void)
{
    glEnable(GL_DEPTH_TEST);

    const auto vso_path = shader_path + "/vertex.vert";
    const auto fso_path = shader_path + "/fragment.frag";

    auto vso =
        ShaderObject(vso_path.c_str(), GL_VERTEX_SHADER);
    auto fso =
        ShaderObject(fso_path.c_str(), GL_FRAGMENT_SHADER);

    vShader = vso.getName();
    fShader = fso.getName();

    program = glCreateProgram();
    if (program == 0) {
        const auto errmsg = "Failed calling glCreateProgram()";
        throw(std::runtime_error(errmsg));
    }

    glAttachShader(program, vso.getName());
    auto gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Failed glAttachShader() " + std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    glAttachShader(program, fso.getName());
    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Failed glAttachShader() " + std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    glLinkProgram(program);

    GLint linked_fine;
    glGetProgramiv(program, GL_LINK_STATUS, &linked_fine);

    gl_error = glGetError();
    // exploit the fact that glLinkProgram and glGetProgramiv here 
    // use overlapping error opcodes with the same meanings;
    if (gl_error == GL_INVALID_VALUE) {
        const auto errmsg = "Program " + std::to_string(program) +
            " is not a value generated by OpenGL";
        throw(std::runtime_error(errmsg));
    } else if (gl_error == GL_INVALID_OPERATION) {
        const auto errmsg = "Program " + std::to_string(program) +
            " is not a program object or program is currently active" +
            " and transform feedack mode is active.";
        throw(std::runtime_error(errmsg));
    } else if (linked_fine == GL_FALSE) {
        const auto errmsg = "Program " + std::to_string(program) +
            " failed to link.";

        GLint info_log_length;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);

        GLchar *log = new GLchar[info_log_length];
        glGetProgramInfoLog(program, info_log_length, NULL, log);

        std::cerr << std::string(log) << std::endl;
        delete[] log;
        throw(std::runtime_error(errmsg));
    }

    glUseProgram(program);
    gl_error = glGetError();
    if (gl_error == GL_INVALID_VALUE) {
        const auto errmsg =
            "Failed glUseProgram() with GL_INVALID_VALUE, program: " +
            std::to_string(program);
        throw(std::runtime_error(errmsg));
    } else if (gl_error == GL_INVALID_OPERATION) {
        const auto errmsg =
            "Failed glUseProgram(program) with GL_INVALID_OPERATION";
        throw(std::runtime_error(errmsg));
    } else if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Failed glUseProgram() with unexpected error code: " +
            std::to_string(gl_error);
        throw(std::runtime_error(errmsg));
    }

    // setup view - proj matrices

    view = glm::lookAt(camera_pos, camera_focus, stage_up);

    auto ratio = 4.0f / 3.0f;
    proj = glm::perspective(FOV, ratio, zNear, zFar);

    glGenVertexArrays(nVAOs, VAO);
    glGenBuffers(nVBOs, VBO);

    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "the problem is here: " + std::to_string(__LINE__) +
            std::to_string(gl_error);
        throw(std::runtime_error(errmsg));
    }

    generate_grid();
    generate_sphere(sphere_triag_level);

    init_textures();

    const auto inclination_axis = glm::vec3(0.0f, 0.0f, 1.0f);
    const float seconds_per_day = 86400;
    for (auto i = 0; i < nPLANETS; ++i) {
        auto is =
            glm::rotate(glm::mat4(), inclination[i], inclination_axis);

        const auto scale_how = avg_radii[i] / avg_radii[EARTH];
        if (i == SUN) {
            scale_incline[i] = glm::scale(is, scale_how / 20.0f);
        } else {
            scale_incline[i] = glm::scale(is, scale_how);
        }

        const auto amount =
            delta_t / seconds_per_day / days2rotate[i] * 360.0f;
        rotate_per_delta_t[i] = amount;
        current_rotation_angle[i] = 0;
    }

    glEnable(GL_CULL_FACE);
    glutSetCursor(GLUT_CURSOR_NONE);
}

void
generate_grid(void)
{    
    glBindVertexArray(VAO[GRID]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[GRID_VBO]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[GRID_COORDS_VBO]); 

    using std::array;
    const auto totLines = 2 * nGrid + 1;
    array<array<array<GLfloat, 5>, totLines>, totLines> grid;

    for (GLfloat i = -nGrid; i <= nGrid; ++i) {
        for (GLfloat j = -nGrid; j <= nGrid; ++j) {
            grid[i + nGrid][j + nGrid] = {
                i , 0.0f, j, 0, 0
            };
        }
    }

    std::array<unsigned int, totGridPoints> grid_coords;
    for (auto i = 0; i < totGridPoints; ++i) {
        grid_coords[i] = i;
    }

    glBufferData(GL_ARRAY_BUFFER, sizeof(grid), grid.begin(),
                 GL_STATIC_DRAW);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(grid_coords),
                 grid_coords.begin(), GL_STATIC_DRAW);

    auto gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "the problem is here: " + std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    const auto modelLoc = glGetUniformLocation(program, "model");
    const auto viewLoc = glGetUniformLocation(program, "view");
    const auto projLoc = glGetUniformLocation(program, "proj");
    const auto uvTexLoc = glGetAttribLocation(program, "uvTex");

    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "the problem is here: " + std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    const auto m = glm::mat4();
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(m));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(proj));

    auto vPositionLoc = glGetAttribLocation(program, "vPosition");
    glVertexAttribPointer(vPositionLoc, 3, GL_FLOAT, GL_FALSE,
                          5 * sizeof(GLfloat), 0);
    glVertexAttribPointer(uvTexLoc, 2, GL_FLOAT, GL_FALSE,
                          5 * sizeof(GLfloat),
                          (const GLvoid *) (3 * sizeof(GLfloat)));

    glEnableVertexAttribArray(vPositionLoc);
    glEnableVertexAttribArray(modelLoc);
    glEnableVertexAttribArray(viewLoc);
    glEnableVertexAttribArray(projLoc);
    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error: " + std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    glEnableVertexAttribArray(uvTexLoc);
    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error: " + std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }
}

void
generate_sphere(unsigned int level)
{
    auto gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error: " + std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    const GLfloat G = (1 + std::sqrt(5)) / 2;
    const std::vector<std::array<GLfloat, 3>> ico_points = {
        {0, G, 1},
        {1, 0, G},
        {G, -1, 0},
        {G, 1, 0},
        {1, 0, -G},
        {0, G, -1},
        {-1, 0, -G},
        {-G, 1, 0},
        {-G, -1, 0},
        {-1, 0, G},
        {0, -G, 1},
        {0, -G, -1}
    };

    const std::vector<GLuint> ico_faces = {
        0, 1, 3,
        0, 3, 5,
        0, 5, 7,
        0, 7, 9,
        0, 9, 1,

        1, 2, 3,
        3, 2, 4,
        3, 4, 5,
        5, 4, 6,
        5, 6, 7,
        7, 6, 8,
        7, 8, 9,
        9, 8, 10,
        9, 10, 1,
        1, 10, 2,

        11, 4, 2,
        11, 2, 10,
        11, 10, 8,
        11, 8, 6,
        11, 6, 4
    };
 
    // now let's triangulate isocohedron until it resembels a sphere;
    glBindVertexArray(VAO[SPHERE]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[SPHERE_VBO]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[SPHERE_COORDS_VBO]);

    std::vector<std::array<GLfloat, 5>> sphere_triag;
    sphere_triag.reserve(ico_faces.size());
    if (level != 0) { // transform to a stupid representation;
        for (auto i = ico_faces.begin(); i != ico_faces.end(); ++i) {
            const auto ico_vertex = ico_points[*i];
            const glm::vec3 ico_v =
                glm::normalize(glm::vec3(ico_vertex[0],
                                         ico_vertex[1],
                                         ico_vertex[2]));
            const float s =
                (std::atan2(ico_vertex[2], ico_vertex[0]) + M_PI) / 2 / M_PI;
            const float t = std::acos(ico_vertex[1]) / M_PI;
            sphere_triag.push_back({ico_v[0], ico_v[1], ico_v[2], s, t});
        }
    }

    for (decltype(level) l = 0; l < level; ++l) {
        std::vector<std::array<GLfloat, 5>> next_sphere;
        next_sphere.reserve(12 * sphere_triag.size());

        const auto start = sphere_triag.begin();
        const auto stop = sphere_triag.end();
        for (auto i = start; i < stop; i += 3) {
            // I am short on short names, hence letters
            const auto x = *i;
            const auto y = *(i + 1);
            const auto z = *(i + 2);

            const auto a = glm::vec3(x[0], x[1], x[2]);
            const auto b = glm::vec3(y[0], y[1], y[2]);
            const auto c = glm::vec3(z[0], z[1], z[2]);

            const auto ab2 = glm::normalize((a + b) / 2.0f);
            const auto ac2 = glm::normalize((a + c) / 2.0f);
            const auto bc2 = glm::normalize((b + c) / 2.0f);

            GLfloat xu = 
                (std::atan2(x[2], x[0]) + M_PI) / 2 / M_PI;
            const GLfloat xv = std::acos(x[1]) / M_PI;
            GLfloat yu =
                (std::atan2(y[2], y[0]) + M_PI) / 2 / M_PI;
            const GLfloat yv = std::acos(y[1]) / M_PI;
            GLfloat zu =
                (std::atan2(z[2], z[0]) + M_PI) / 2 / M_PI;
            const GLfloat zv = std::acos(z[1]) / M_PI;

            auto seam = is_seam(xu, yu, zu);
            if (seam == 1) {
                xu = 1 - xu;
            } else if (seam == 2) {
                yu = 1 - yu;
            } else if (seam == 3) {
                zu = 1 - zu;
            }

            float ab2u =
                (std::atan2(ab2[2], ab2[0]) + M_PI) / 2 / M_PI;
            const float ab2v = std::acos(ab2[1]) / M_PI;
            float ac2u =
                (std::atan2(ac2[2], ac2[0]) + M_PI) / 2 / M_PI;
            const float ac2v = std::acos(ac2[1]) / M_PI;
            float bc2u =
                (std::atan2(bc2[2], bc2[0]) + M_PI) / 2 / M_PI;
            const float bc2v = std::acos(bc2[1]) / M_PI;

            seam = is_seam(ab2u, ac2u, bc2u);
            if (seam == 1) {
                ab2u = 1 - ab2u;
            } else if (seam == 2) {
                ac2u = 1 - ac2u;
            } else if (seam == 3) {
                bc2u = 1 - bc2u;
            }

            seam = is_seam(xu, ab2u, ac2u);
            if (seam == 1) {
                xu = 1 - xu;
            } else if (seam == 2) {
                ab2u = 1 - ab2u;
            } else if (seam == 3) {
                ac2u = 1 - ac2u;
            }

            seam = is_seam(ab2u, yu, bc2u);
            if (seam == 1) {
                ab2u = 1 - ab2u;
            } else if (seam == 2) {
                yu = 1 - yu;
            } else if (seam == 3) {
                bc2u = 1 - bc2u;
            }

            seam = is_seam(ac2u, bc2u, zu);
            if (seam == 1) {
                ac2u = 1 - ac2u;
            } else if (seam == 2) {
                bc2u = 1 - bc2u;
            } else if (seam == 3) {
                zu = 1 - zu;
            }

            next_sphere.push_back({x[0], x[1], x[2], xu, xv});
            next_sphere.push_back({ab2[0], ab2[1], ab2[2], ab2u,ab2v});
            next_sphere.push_back({ac2[0], ac2[1], ac2[2], ac2u,ac2v});

            next_sphere.push_back({y[0], y[1], y[2], yu, yv});
            next_sphere.push_back({bc2[0], bc2[1], bc2[2], bc2u,bc2v});
            next_sphere.push_back({ab2[0], ab2[1], ab2[2], ab2u,ab2v});

            next_sphere.push_back({z[0], z[1], z[2], zu, zv});
            next_sphere.push_back({ac2[0], ac2[1], ac2[2], ac2u,ac2v});
            next_sphere.push_back({bc2[0], bc2[1], bc2[2], bc2u,bc2v});

            next_sphere.push_back({ac2[0], ac2[1], ac2[2], ac2u,ac2v});
            next_sphere.push_back({ab2[0], ab2[1], ab2[2], ab2u,ab2v});
            next_sphere.push_back({bc2[0], bc2[1], bc2[2], bc2u,bc2v});
        }

        // let's rely on the move assignment operator;
        sphere_triag = next_sphere;
    }

    const auto sphere_triag_bytes =
        5 * sphere_triag.size() * sizeof(GLfloat);

    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error: " + std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    glBufferData(GL_ARRAY_BUFFER, sphere_triag_bytes,
                 &sphere_triag[0], GL_STATIC_DRAW);

    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error: " + std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    const auto modelLoc = glGetUniformLocation(program, "model");
    const auto viewLoc = glGetUniformLocation(program, "view");
    const auto projLoc = glGetUniformLocation(program, "proj");
    const auto camLoc = glGetUniformLocation(program, "camera_pos");
    const auto is_sunLoc = glGetUniformLocation(program,"is_sun");
    const auto materialLoc = glGetUniformLocation(program,"material");

    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error: " + std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    const auto m = glm::mat4();
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(m));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(proj));
    glUniform3f(camLoc, camera_pos[0], camera_pos[1], camera_pos[2]);
    glUniform4f(materialLoc, 1.0f, 1.0f, 1.0f, 1.0f);

    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error: " + std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    const auto vPositionLoc =
        glGetAttribLocation(program, "vPosition");
    glVertexAttribPointer(vPositionLoc, 3, GL_FLOAT, GL_FALSE,
                          5 * sizeof(GLfloat), 0);

    const auto uvTexLoc = glGetAttribLocation(program, "uvTex");
    if (uvTexLoc == -1) {
        std::cerr << "uvTexLoc not found?" << std::endl;
    }

    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error: " + std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    const auto offset = 3 * sizeof(GLfloat);
    glVertexAttribPointer(uvTexLoc, 2, GL_FLOAT, GL_FALSE,
                          5 * sizeof(GLfloat), (const GLvoid *)offset);

    gl_error = glGetError();
    if (gl_error == GL_INVALID_VALUE) {
        const auto errmsg = "glVertexAttribPointer() at " +
            std::to_string(__LINE__) + " reports GL_INVALID_VALUE";
        throw(std::runtime_error(errmsg));
    } else if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error " + std::to_string(gl_error) +
            " is here: " + std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    glEnableVertexAttribArray(is_sunLoc);
    glEnableVertexAttribArray(camLoc);
    glEnableVertexAttribArray(materialLoc);

    glEnableVertexAttribArray(vPositionLoc);
    glEnableVertexAttribArray(uvTexLoc);
    glEnableVertexAttribArray(modelLoc);
    glEnableVertexAttribArray(viewLoc);
    glEnableVertexAttribArray(projLoc);

    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error: " + std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }
}

unsigned int
is_seam(const GLfloat& a, const GLfloat& b, const GLfloat& c)
{
    const auto left = 0.3;
    const auto right = 0.7;
    if (a < left and b > right and c > right) {
        return 1;
    } else if (a > right and b < left and c > right) {
        return 2;
    } else if (a > right and b > right and c < left) {
        return 3;
    } else if (a > right and b < left and c < left) {
        return 1;
    } else if (a < left and b > right and c < left) {
        return 2;
    } else if (a < left and b < left and c > right) {
        return 3;
    }

    return 0;
}
