#include "main.h"

void display_grid(void);
void display_isocoh(void);
void display_planet(int which, glm::mat4 model);

void
display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (show_grid) {
        display_grid();
    }

    if (wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    using std::pow;
    for (int i = 0; i < nPLANETS; ++i) {
        auto model_length = 0.0f;
        auto direction = glm::vec3(0.0f);
        const auto relcoord = coords[i] - coords[SUN];
        if (relcoord != glm::vec3(0.0f)) {
            direction = glm::normalize(relcoord);
            if (schematic) {
                // ROOTPOW is a global constant from main.h;
                model_length = pow(glm::length(relcoord), ROOTPOW);
            } else {
                // DENOM is a global constant from main.h;
                model_length = glm::length(relcoord) / DENOM;
            }
        }

        const glm::vec3 start_from = model_length * direction;
        auto m = glm::translate(glm::mat4(), start_from);

        m = m * scale_incline[i];

        const auto angle = current_rotation_angle[i];
        m = glm::rotate(m, angle, stage_up);

        if (not suspend_animation) {
            current_rotation_angle[i] += rotate_per_delta_t[i];
            if (current_rotation_angle[i] > 360) {
                current_rotation_angle[i] = 0;
            }
        }

        display_planet(i, m);
    }

    unsigned it = 0;
    for (; not suspend_animation and it < iterations; ++it) {
        glm::vec3 new_coords[nPLANETS];
        for (auto i = 0; i < nPLANETS; ++i) {
            new_coords[i] = coords[i];
            const auto position = coords[i];
            glm::vec3 a = glm::vec3(0.0f, 0.0f, 0.0f);

            for (auto j = 0; j < nPLANETS; ++j) {
                if (i == j) {continue;}

                const auto direction =
                    glm::normalize(coords[j] - position);

                const double length = 1000.0f *
                    glm::length(coords[j] - position);

                const float a_amount = G * masses[j] / pow(length, 2);
                // std::cerr << a_amount << std::endl;
                a += a_amount * direction;
            }

            const auto meters = 0.5f * a * float(pow(delta_t, 2))
                + 1000.0f * speeds[i] * delta_t;
            new_coords[i] += meters / 1000.0f;

            speeds[i] += a * delta_t / 1000.0f;
        }

        for (auto i = 0; i < nPLANETS; ++i) {
            coords[i] = new_coords[i];
        }
    }

    glutSwapBuffers();
}

void
display_planet(int which, glm::mat4 model)
{
    const std::string prototype = __func__;
    auto gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg = prototype + " reports error code: " +
            std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    glBindVertexArray(VAO[SPHERE]);
    glBindTexture(GL_TEXTURE_2D, TEX[which]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[SPHERE]);

    const auto modelLoc = glGetUniformLocation(program, "model");
    const auto viewLoc = glGetUniformLocation(program, "view");
    const auto projLoc = glGetUniformLocation(program, "proj");
    const auto camLoc = glGetUniformLocation(program, "camera_pos");
    const auto is_sunLoc = glGetUniformLocation(program,"is_sun");
    const auto materialLoc = glGetUniformLocation(program,"material");

    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(proj));
    glUniform3f(camLoc, camera_pos[0], camera_pos[1], camera_pos[2]);

    if (which == 0) {
        glUniform1i(is_sunLoc, 0);
    } else {
        glUniform1i(is_sunLoc, 1);
    }

    if (which == SUN || which == JUPITER || which == SATURN
        || which == URANUS) {
        glUniform4f(materialLoc, 1.0f, 1.0f, 1.0f, 5.0f);
    } else {
        glUniform4f(materialLoc, 1.0f, 1.0f, 0.8f, 3.0f);
    }

    const auto nVertices = 20 * 3 * std::pow(4, sphere_triag_level);
    glDrawArrays(GL_TRIANGLES, 0, nVertices);

    gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg = prototype + " reports error code: " +
            std::to_string(gl_error) + " at line: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }
}

void display_grid(void)
{
    glBindVertexArray(VAO[GRID]);

    const auto totLines = 2 * nGrid + 1;
    std::array<GLint, totLines> start_from;
    std::array<GLsizei, totLines> n2draw;

    for (auto i = 0, j = 0; i < totLines; ++i, j += totLines) {
        start_from[i] = j;
        n2draw[i] = totLines;
    }

    auto modelLoc = glGetUniformLocation(program, "model");
    auto viewLoc = glGetUniformLocation(program, "view");
    auto projLoc = glGetUniformLocation(program, "proj");

    auto smodel = glm::mat4();
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(smodel));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(proj));

    glMultiDrawArrays(GL_LINE_STRIP, start_from.begin(),
                      n2draw.begin(), totLines);
}
