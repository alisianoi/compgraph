#include "main.h"

ILuint IMGS[nPLANETS];

void
init_textures(void)
{
    const std::string prototype = __func__;
    std::cerr << prototype << "() begins" << std::endl;

    auto gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        const auto errmsg =
            "Error: " + std::to_string(gl_error) + " is here: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    const auto sun_path = textures_path + "/sun.jpg";
    const auto mercury_path = textures_path + "/mercury.jpg";
    const auto venus_path = textures_path + "/venus.jpg";
    const auto earth_path = textures_path + "/earth.jpg";
    const auto mars_path = textures_path + "/mars.jpg";
    const auto jupiter_path = textures_path + "/jupiter.jpg";
    const auto saturn_path = textures_path + "/saturn.png";
    const auto uranus_path = textures_path + "/uranus.jpg";
    const auto neptune_path = textures_path + "/neptune.jpg";
    // and since there is no decent texture for pluto...
    const auto pluto_path = neptune_path;

    const std::array<std::string, nPLANETS> texture_paths =
        {
            sun_path, mercury_path, venus_path, earth_path, mars_path,
            jupiter_path, saturn_path, uranus_path, neptune_path,
            pluto_path
        };

    ilInit();
    auto il_error = ilGetError();
    if (il_error != IL_NO_ERROR) {
        const auto errmsg =
            "IL Error: " + std::to_string(il_error) + " is here: " +
            std::to_string(__LINE__);
        throw(std::runtime_error(errmsg));
    }

    glBindVertexArray(VAO[SPHERE]);
    glGenTextures(nPLANETS, TEX);
    ilGenImages(nPLANETS, IMGS);

    for (int planet = 0; planet < nPLANETS; ++planet) {
        glBindTexture(GL_TEXTURE_2D, TEX[planet]);

        ilBindImage(IMGS[planet]);
        ilLoadImage(texture_paths[planet].c_str());

        il_error = ilGetError();
        if (il_error != IL_NO_ERROR) {
            const auto errmsg =
                "IL Error: " + std::to_string(il_error) + " is here: " +
                std::to_string(__LINE__) + " on planet: " +
                std::to_string(planet);
            throw(std::runtime_error(errmsg));
        }

        const auto w = ilGetInteger(IL_IMAGE_WIDTH);
        const auto h = ilGetInteger(IL_IMAGE_HEIGHT);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB,
                     GL_UNSIGNED_BYTE, ilGetData());

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                        GL_NEAREST_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                        GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                        GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                        GL_CLAMP_TO_BORDER);

        gl_error = glGetError();
        if (gl_error != GL_NO_ERROR) {
            const auto errmsg =
                "GL Error: " + std::to_string(gl_error) + " is here: " +
                std::to_string(__LINE__);
            throw(std::runtime_error(errmsg));
        }

        glGenerateMipmap(GL_TEXTURE_2D);
    }

    std::cerr << prototype << "() finished" << std::endl;
}
