#include "main.h"

void
reshape(int width, int height)
{
    const std::string prototype = __func__;
    // std::cerr << __func__ << "() takes control" << std::endl;

    glViewport(0, 0, width, height);

    const auto ratio = float(width) / height;
    glm::perspective(FOV, ratio, zNear, zFar);

    windowWidth = width;
    windowHeight = height;

    glutWarpPointer(width / 2, height / 2);
    mouse_x = width / 2;
    mouse_y = height / 2;

    // std::cerr << __func__ << "() drops control" << std::endl;
}
