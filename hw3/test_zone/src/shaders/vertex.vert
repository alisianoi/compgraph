#version 140

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

in vec2 uvTex;
out vec2 uv;

in vec3 vPosition;

out vec3 normal;
out mat4 model_mat;

void
main()
{
    gl_Position = proj * view * model * vec4(vPosition, 1.0);

    uv = uvTex;
    normal = vPosition;
    model_mat = model;
}
