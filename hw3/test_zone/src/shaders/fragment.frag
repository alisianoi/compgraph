#version 140

uniform sampler2D tex;
uniform vec3 camera_pos;
uniform int is_sun; // 0 if SUN, >0 otherwise;
uniform vec4 material;

in vec2 uv;

in vec3 normal;
in mat4 model_mat;

out vec4 fColor;

void
main()
{
    vec4 center = vec4(model_mat[3].xyz, 1.0f);
    mat4 scale_rot = model_mat;
    scale_rot[3].xyz = vec3(0.0f);

    vec4 radius = scale_rot * vec4(normal, 1.0f);
    vec4 normal_rot = normalize(scale_rot * vec4(normal, 1.0f));

    vec4 L;
    if (is_sun == 0) {
        L = normalize(radius - center);
    } else {
        L = normalize(-radius - center);
    }

    vec4 V;
    if (is_sun == 0) {
        V = normalize(L + vec4(camera_pos, 1.0f));
    } else {
        V = normalize(-L - vec4(camera_pos, 1.0f));
    }

    vec4 R = reflect(L, normal_rot);

    float diffuse = clamp(dot(L, normal_rot), 0, 1);
    float specular = pow(clamp(dot(R, V), 0, 1), material[3]);
    float I = 0.4 * material[0];
    // and now follow this article from wikipedia
    // http://en.wikipedia.org/wiki/Phong_reflection_model
    // so as to avoid having a disk of light around a planet;
    // make Phong more realistic;
    if (diffuse > 0) {
        I += material[1] * diffuse + material[2] * specular;
    }

    fColor = texture(tex, uv) * I * vec4(1.0f, 1.0f, 1.0f, 1.0f);
}
