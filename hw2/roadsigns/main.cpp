#include <string>
#include <vector>
#include <fstream>
#include <cassert>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <functional>
#include <algorithm>

#include "classifier.h"
#include "EasyBMP/EasyBMP.h"
#include "liblinear-1.93/linear.h"
#include "argvparser/argvparser.h"

using std::string;
using std::vector;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::cout;
using std::cerr;
using std::endl;
using std::array;

using CommandLineProcessing::ArgvParser;

typedef vector<pair<BMP*, int>> TDataSet;
typedef vector<pair<string, int>> TFileList;
typedef vector<pair<vector<float>, int>> TFeatures;

class ProblemParams
{
public:
    array<int, 2> _szCell;
    array<int, 2> _szBlock;
    array<int, 2> _hwStride;
    int _margin;

    int _nOfBins;
    bool _sign;

    TClassifierParams _params;
    std::function<double(const std::vector<float> &)> _metric;

    ProblemParams()
        : _szCell({0, 0}), _szBlock({0, 0}), _hwStride({0, 0}),
          _margin(0), _nOfBins(0), _sign(false), _params(), _metric()
    {}
    ProblemParams(const std::array<int, 2> &szCell,
                  const std::array<int, 2> &szBlock,
                  const std::array<int, 2> &hwStride,
                  const int margin, const int nOfBins,
                  const bool sign, TClassifierParams params,
                  std::function<double(const std::vector<float> &)> metric)
        : _szCell(szCell), _szBlock(szBlock), _hwStride(hwStride), _margin(margin),
          _nOfBins(nOfBins), _sign(sign), _metric(metric), _params(params)
    {}

    ProblemParams& operator= (const ProblemParams& src)
    {
        _szCell = src._szCell;
        _szBlock = src._szBlock;
        _hwStride = src._hwStride;
        _margin = src._margin;

        _nOfBins = src._nOfBins;
        _sign = src._sign;

        _params = src._params;
        _metric = src._metric;
    }

    friend std::ostream& operator<<(std::ostream &out, const ProblemParams &pp)
    {
        out << "cells: " << pp._szCell[0] << " x " << pp._szCell[1]
            << ", blocks: " << pp._szBlock[0] << " x " << pp._szBlock[1]
            << ", stride: " << pp._hwStride[0] << " x " << pp._hwStride[1]
            << ", margin: " << pp._margin << ", bins: " << pp._nOfBins
            << " sign: " << pp._sign << ", params.C: " << pp._params.C
            << " metric: ";

        const std::vector<float> test = {3, 4};
        const auto result = pp._metric(test);
        if (round(result) == 7) {
            out << "sum";
        } else if (round(result) == 4) {
            out << "max";
        } else if (round(result) == -1) {
            out << "none";
        } else {
            out << "euclid";
        }
        out << std::endl;
    }
};

void PredictData(const string& data_file,
                 const string& model_file,
                 const string& prediction_file,
                 const ProblemParams& pp);

typedef std::map<string, string> mTLabels;

void
LoadLabels(const string& data_file, mTLabels* labels) {
    ifstream stream(data_file.c_str());

    string filename;
    int label;

    int char_idx = data_file.size() - 1;
    for (; char_idx >= 0; --char_idx)
        if (data_file[char_idx] == '/' || data_file[char_idx] == '\\')
            break;
    const string data_path = data_file.substr(0,char_idx+1);

    while(!stream.eof() && !stream.fail()) {
        stream >> filename >> label;
        if (filename.size())
            (*labels)[data_path + filename] = label;
    }

    stream.close();
}

double
TestLabels(const mTLabels& gt_labels, const mTLabels& predicted_labels)
{
    int correct_predictions = 0;    

    if (gt_labels.size() != predicted_labels.size()) {
        cout << "Error! Files with predicted and ground truth labels "
             << "have different number of samples: "
             << gt_labels.size() << " and " << predicted_labels.size()
             << " respectively." << endl;
        return -1;
    }

    if (!gt_labels.size()) {
        cout << "Error! Dataset is empty.";
        return -1;
    }

    for (mTLabels::const_iterator predicted_it = predicted_labels.begin();
         predicted_it != predicted_labels.end();
         ++predicted_it) {
        string sample = predicted_it->first;
        mTLabels::const_iterator gt_it = gt_labels.find(sample);
        if (gt_it == gt_labels.end()) {
            cout << "Error! File " << sample
                 << " has no ground truth label." << endl;
            return -1;
        }

        if (predicted_it->second == gt_it->second)
            ++correct_predictions;
    }
    return double(correct_predictions) / gt_labels.size();
}

// eWidth and eHeight stand for extraWidth and extraHeight
vector<vector<float>>
mirror(const BMP &image, int eWidth, int eHeight)
{
    const auto w = image.GetWidth();
    const auto h = image.GetHeight();

    if (eWidth > w || eHeight > h) {
        std::string msg = "too much expansion has been requested";
        throw msg;
    }
    
    vector<vector<float>>
        canvas(w + 2 * eWidth, vector<float>(h + 2 * eHeight));

    // copy center
    for (auto i = 0; i < w; ++i) {
        for (auto j = 0; j < h; ++j) {
            canvas[eWidth + i][eHeight + j] = image(i, j)->Red;
        }
    }

    // upper nineth
    for (auto i = 0; i < w; ++i) {
        for (auto j = 0; j < eHeight; ++j) {
            canvas[eWidth + i][eHeight - 1 - j] = image(i, j)->Red;
        }
    }

    // lower nineth
    for (auto i = 0; i < w; ++i) {
        for (auto j = 0; j < eHeight; ++j) {
            canvas[eWidth + i][eHeight + h + j] = image(i, h - 1 - j)->Red;
        }
    }

    // left triple
    for (auto i = 0; i < eWidth; ++i) {
        for (auto j = 0; j < h + 2 * eHeight; ++j) {
            canvas[i][j] = canvas[2 * eWidth - 1 - i][j];
        }
    }
    
    // right triple
    for (auto i = 0; i < eWidth; ++i) {
        for (auto j = 0; j < h + 2 * eHeight; ++j) {
            canvas[w + eWidth + i][j] = canvas[w + eWidth - 1 - i][j];
        }
    }

    return canvas;
}

vector<vector<float>>
convolve(const BMP &image, const vector<vector<float>> &kernel)
{
    const auto height = image.GetHeight();
    const auto width = image.GetWidth();
    const auto kWidth = kernel.size();
    const auto kHeight = kernel[0].size();

    const size_t nHeight = height + kHeight - 1;
    const size_t nWidth = width + kWidth - 1;

    vector<vector<float>> result(width, vector<float>(height));

    auto canvas = mirror(image, kWidth / 2, kHeight / 2);

    for (auto i = 0; i < width; ++i) {
        for (auto j = 0; j < height; ++j) {
            float val = 0;
            for (auto k = 0; k < kWidth; ++k) {
                for (auto l = 0; l < kHeight; ++l) {
                    val += canvas[i + k][j + l] * kernel[k][l];
                }
            }
            result[i][j] = val;
        }
    }

    return result;
}

vector<vector<float>>
sobel_hor(const BMP &image)
{
    vector<vector<float>> kernel = {{-1, 0, 1}};
    
    // vector<vector<float>> kernel = {
    //     {1, 2, 1},
    //     {0, 0, 0},
    //     {-1, -2, -1}
    // };
    return convolve(image, kernel);
}

vector<vector<float>>
sobel_ver(const BMP &image)
{
    vector<vector<float>> kernel = {{-1}, {0}, {1}};
    // vector<vector<float>> kernel = {
    //     {-1, 0, 1},
    //     {-2, 0, 2},
    //     {-1, 0, 1}
    // };
    return convolve(image, kernel);
}

// `sw` and `sh` stand for `start width` and height respectively
vector<float>
cell2hist(const vector<vector<float>> &gradient,
          const vector<vector<float>> &angle,
          const int sw, const int sh,
          const int w, const int h,
          const int bins, const bool sign)
{
    // first initialize s, then make it constant
    double s = 0;
    if (sign) {
        s = 2 * M_PI / bins;
    } else {
        s = M_PI / bins;
    }
    const double step = s;

    vector<float> hist(bins + 1);
    std::fill(hist.begin(), hist.end(), 0);

    // sa stands for "starting angle"
    // sw and sh stand for starting width and height
    const auto sa = sign ? -M_PI : 0;
    for (auto i = sw; i < sw + w; ++i) {
        for (auto j = sh; j < sh + h; ++j) {
            // a stadns for "angle"
            const auto a = sign ? angle[i][j] : abs(angle[i][j]);
            auto l = 0;
            for (auto k = sa; k < M_PI; k += step, ++l) {
                if (k <= a && a <= k + step) {
                    // consdier weighting otherwise!
                    // Dalal and Triggs suggest taking plain value
                    hist[l] = gradient[i][j];
                }
            }
        }
    }

    // int k; std::cerr << "cell histogram looks like this:" <<
    // std::endl; for (auto i : hist) {std::cerr << i << " ";}
    // std::cerr << std::endl; std::cin >> k;
    return hist;
}

double
euclid_metric(const vector<float> &v)
{
    using std::pow;
    double norm = 0;
    for (auto i : v) {
        norm += pow(i, 2);
    }

    return pow(norm, 0.5);
}

double
max_metric(const vector<float> &v)
{
    return *std::max_element(v.begin(), v.end());
}

double
sum_metric(const vector<float> &v)
{
    return std::accumulate(v.begin(), v.end(), 0);
}

double
no_metric(const vector<float> &v)
{
    return -1;
}

// Histogram of Oriented Gradients implementation here
// think on return value -- it might not be that long to copy
// but there will be plenty of calls to HOG... 1000+ calls...
// the image that gets received is already grayscale
// ppc stands for pixels per cell
// cpb stands for cells per block
std::vector<float>
HOG(BMP &image, const ProblemParams &pp)
{
    const auto margin = pp._margin;
    const auto w = image.GetWidth() - 2 * margin;
    const auto h = image.GetHeight() - 2 * margin;

    auto hor = sobel_hor(image);
    auto ver = sobel_ver(image);

    vector<vector<float>> gradient(h, vector<float>(w));
    vector<vector<float>> angle(h, vector<float>(w));

    const auto eps = 1e-7;
    for (auto i = 0; i < w; ++i) {
        for (auto j = 0; j < h; ++j) {
            gradient[i][j] =
                pow(pow(hor[i + margin][j + margin], 2) + pow(ver[i + margin][j + margin], 2), 0.5);
            angle[i][j] =
                atan2(ver[i + margin][j + margin], hor[i + margin][j + margin]);
        }
    }

    // width pixels per cell
    const auto wppc = pp._szCell[0];
    // height pixels per cell
    const auto hppc = pp._szCell[1];
    // width pixels per block
    const auto wppb = wppc * pp._szBlock[0];
    // height pixels per block
    const auto hppb = hppc * pp._szBlock[1];
    // width stride of blocks
    const auto wstride = pp._hwStride[0] * wppc;
    // height stride of blocks
    const auto hstride = pp._hwStride[1] * hppc;
    std::function<double(const std::vector<float> &)> metric = pp._metric;
    vector<float> features;
    for (auto i = 0; i + wppb <= w; i += wstride) {
        // concatenated block histogram
        vector<float> bh;
        for (auto j = 0; j + hppb <= h; j += hstride) {
            // cell histogram
            vector<float> ch;
            for (auto k = 0; k < wppb; k += wppc) {
                for (auto l = 0; l < hppb; l += hppc) {
                    ch = cell2hist(gradient, angle, i + k, j + l,
                                   wppc, hppc, pp._nOfBins, pp._sign);
                }
            }
            bh.insert(bh.end(), ch.begin(), ch.end());
        }
        // consider normalizing block!
        const auto eps = 1e-7;
        const auto norm = metric(bh);
        if (norm != -1 && norm > eps) {
            for (auto m = 0; m < bh.size(); ++m) {
                bh[m] /= norm;
            }
        } else if (abs(norm) < eps) {
        } else if (norm < 0) {
        }

        features.insert(features.end(), bh.begin(), bh.end());
    }

    return features;
}

void
grayscale(BMP &image)
{
    const auto height = image.TellHeight();
    const auto width = image.TellWidth();
    
    for (auto i = 0; i < width; ++i) {
        for (auto j = 0; j < height; ++j) {
            RGBApixel p = image.GetPixel(i, j);
            auto nColor =
                0.299 * p.Red + 0.587 * p.Green + 0.114 * p.Blue;
            p.Red = nColor;
            p.Green = nColor;
            p.Blue = nColor;
            image.SetPixel(i, j, p);
        }
    }
}

// Load list of files and its labels from 'data_file' and
// stores it in 'file_list'
void LoadFileList(const string& data_file, TFileList* file_list) {
    ifstream stream(data_file.c_str());

    string filename;
    int label;    

    int char_idx = data_file.size() - 1;
    for (; char_idx >= 0; --char_idx)
        if (data_file[char_idx] == '/' || data_file[char_idx] == '\\')
            break;
    string data_path = data_file.substr(0,char_idx+1);
    
    while(!stream.eof() && !stream.fail()) {
        stream >> filename >> label;
        if (filename.size())
            file_list->push_back(make_pair(data_path + filename, label));
    }

    stream.close();
}

// Load images by list of files 'file_list' and store them in 'data_set'
void LoadImages(const TFileList& file_list, TDataSet* data_set) {
    for (size_t img_idx = 0; img_idx < file_list.size(); ++img_idx) {
        // Create image
        BMP* image = new BMP();
        // Read image from file
        image->ReadFromFile(file_list[img_idx].first.c_str());
        // convert image to grayscale explicitly
        grayscale(*image);
        // Add image and it's label to dataset
        // consider preallocating for speed
        data_set->push_back(make_pair(image, file_list[img_idx].second));
    }
}

// Save result of prediction to file
void SavePredictions(const TFileList& file_list,
                     const TLabels& labels, 
                     const string& prediction_file) {
    // Check that list of files and list of labels has equal size 
    assert(file_list.size() == labels.size());
    // Open 'prediction_file' for writing
    ofstream stream(prediction_file.c_str());

    // Write file names and labels to stream
    for (size_t image_idx = 0; image_idx < file_list.size(); ++image_idx)
        stream << file_list[image_idx].first << " " << labels[image_idx] << endl;
    stream.close();
}

// Extract features from dataset.
// You should implement this function by yourself =)
void ExtractFeatures(const TDataSet& data_set, TFeatures* features, const ProblemParams &pp) {
    const auto dsLength = data_set.size();
    for (size_t image_idx = 0; image_idx < dsLength; ++image_idx) {
        const auto label = data_set[image_idx].second;
        vector<float> one_image_features = 
            HOG(*data_set[image_idx].first, pp);

        features->push_back(make_pair(one_image_features, label));
    }
}

// Clear dataset structure
void ClearDataset(TDataSet* data_set) {
        // Delete all images from dataset
    for (size_t image_idx = 0; image_idx < data_set->size(); ++image_idx)
        delete (*data_set)[image_idx].first;
        // Clear dataset
    data_set->clear();
}

// Train SVM classifier using data from 'data_file' and save trained
// model to 'model_file'
void TrainClassifier(const string& data_file, const string& model_file) {
    TFileList file_list;
    TDataSet data_set;

    LoadFileList(data_file, &file_list);
    LoadImages(file_list, &data_set);

    TModel model;
    TClassifierParams params;

    // // start commenting from here
    // using std::array;
    // double score = 0;
    // ProblemParams pp;
    // // assume all images have matching dimensions
    // const auto width = (*data_set[0].first).GetWidth();
    // const auto height = (*data_set[0].first).GetHeight();

    // const array<int, 4> nBins = {18, 10, 9, 8};
    // const array<float, 5> C = {0.2, 0.5, 0.7, 0.75, 0.8};
    // const array<std::function<double(const std::vector<float> &)>, 3> metrics = {
    //     euclid_metric,
    //     max_metric,
    //     sum_metric,
    // };
    
    // const std::string dataPath =
    //     "/home/all3fox/Code/compgraph/hw2/roadsigns/data/";
    // const std::string tdPath = dataPath + "test_labels.txt";
    // const std::string pLabels = "predicted_labels.txt";

    // const std::string logdir =
    //     "/home/all3fox/Code/compgraph/hw2/roadsigns/log/";
    // // each and every result goes into here
    // const std::string verbose_log = logdir + "verbose.log";
    // // each model which was more successful than some other, goes here
    // const std::string success_log = logdir + "success.log";
    // ofstream vlog(verbose_log);
    // ofstream slog(success_log);

    // mTLabels gt_labels;
    // LoadLabels(tdPath, &gt_labels);

    // const auto maxside = std::max(width, height);
    // for (auto margin = maxside / 5; margin >= 0; --margin) {
    //     const auto w = width - 2 * margin;
    //     const auto h = height - 2 * margin;
    //     for (auto wCell = 1; wCell < w / 2; ++wCell) {
    //         for (auto hCell = wCell; hCell < h / 2; ++hCell) {
    //             const auto maxBlockWidth = w / wCell;
    //             const auto maxBlockHeight = h / hCell;
    //             const array<int, 2> szCell = {wCell, hCell};

    //             for (auto wBlock = 1; wBlock <= maxBlockWidth; ++wBlock) {
    //                 // consider const auto hBlock = wBlock or an honest rectangle
    //                 for (auto hBlock = wBlock; hBlock <= maxBlockWidth; ++hBlock) {
    //                     const array<int, 2> szBlock = {wBlock, hBlock};

    //                     for (auto hStride = 1; hStride <= hBlock; ++hStride) {
    //                         // consider const auto wStride = hStride or honest rectangle
    //                         for (auto wStride = hStride; wStride <= wBlock; ++wStride) {
    //                             const array<int, 2> hwStride = {wStride, hStride};

    //                             for (auto nOfBins : nBins) {
    //                                 for (auto i = 0; i < metrics.size(); ++i) {
    //                                     params.C = 0.7;
    //                                     ProblemParams cur_pp(szCell, szBlock, hwStride, margin, nOfBins, true, params, metrics[i]);

    //                                     TFeatures features;
    //                                     ExtractFeatures(data_set, &features, cur_pp);

    //                                     double prev_score = 0;
    //                                     for (auto c : C) {
    //                                         cur_pp._params.C = c;
    //                                         std::cerr << cur_pp;

    //                                         TClassifier classifier(cur_pp._params);
    //                                         classifier.Train(features, &model);

    //                                         model.Save(model_file);

    //                                         PredictData(tdPath, model_file, pLabels, cur_pp);

    //                                         mTLabels mpLabels;
    //                                         LoadLabels(pLabels, &mpLabels);

    //                                         const auto cur_score =
    //                                             TestLabels(gt_labels, mpLabels);
    //                                         prev_score == 0 ? cur_score : (prev_score == cur_score ? -1 : cur_score);
    //                                         if (cur_score < score) {
    //                                             vlog << std::fixed
    //                                                  << cur_score << ", " << cur_pp;
    //                                         } else {
    //                                             vlog << std::fixed
    //                                                  << cur_score << ", " << cur_pp;
    //                                             slog << std::fixed
    //                                                  << cur_score << ", " << cur_pp;

    //                                             score = cur_score;
    //                                             pp = cur_pp;
    //                                         }
    //                                         if (round(prev_score) == -1) {
    //                                             vlog << "varying c has no effect, skipping" << std::endl;
    //                                             break;
    //                                         }
    //                                     }
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    // //stop commenting here

    // start uncommenting here
    const std::array<int, 2> szCell = {1, 1};
    const std::array<int, 2> szBlock = {1, 6};
    const std::array<int, 2> hwStride = {1, 1};
    const auto margin = 6;
    const auto bins = 9;
    const bool sign = 1;
    params.C = 0.7;
    auto metric = sum_metric;
    ProblemParams pp =
        ProblemParams(szCell, szBlock, hwStride,
                      margin, bins, sign, params, metric);

    TFeatures features;
    ExtractFeatures(data_set, &features, pp);

    TClassifier classifier(pp._params);
    classifier.Train(features, &model);

    model.Save(model_file);

    ClearDataset(&data_set);
}

// Predict data from 'data_file' using model from 'model_file' and
// save predictions to 'prediction_file'
void PredictData(const string& data_file,
                 const string& model_file,
                 const string& prediction_file,
                 const ProblemParams &pp) {
    TFileList file_list;
    TDataSet data_set;

    TFeatures features;
    TLabels labels;

    LoadFileList(data_file, &file_list);
    LoadImages(file_list, &data_set);

    ExtractFeatures(data_set, &features, pp);

    TClassifierParams params = pp._params;
    TClassifier classifier = TClassifier(params);

    TModel model;

    model.Load(model_file);

    classifier.Predict(features, model, &labels);

    SavePredictions(file_list, labels, prediction_file);

    ClearDataset(&data_set);
}

int main(int argc, char** argv) {
    ArgvParser cmd;
    cmd.setIntroductoryDescription("Machine graphics course, task 2. CMC MSU, 2013.");
    cmd.setHelpOption("h", "help", "Print this help message");
    cmd.defineOption("data_set", "File with dataset",
                     ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
    cmd.defineOption("model", "Path to file to save or load model",
                     ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
    cmd.defineOption("predicted_labels", "Path to file to save prediction results",
                     ArgvParser::OptionRequiresValue);
    cmd.defineOption("train", "Train classifier");
    cmd.defineOption("predict", "Predict dataset");
        
    // Add options aliases
    cmd.defineOptionAlternative("data_set", "d");
    cmd.defineOptionAlternative("model", "m");
    cmd.defineOptionAlternative("predicted_labels", "l");
    cmd.defineOptionAlternative("train", "t");
    cmd.defineOptionAlternative("predict", "p");

    // Parse options
    int result = cmd.parse(argc, argv);

    // Check for errors or help option
    if (result) {
        cout << cmd.parseErrorDescription(result) << endl;
        return result;
    }

    // Get values 
    string data_file = cmd.optionValue("data_set");
    string model_file = cmd.optionValue("model");
    bool train = cmd.foundOption("train");
    bool predict = cmd.foundOption("predict");

    if (train) {
        TrainClassifier(data_file, model_file);
    }
    // NOTE: can we both train and predict?
    if (predict) {
        // You must declare file to save images
        if (!cmd.foundOption("predicted_labels")) {
            cerr << "Error! Option --predicted_labels not found!" << endl;
            return 1;
        }
        // File to save predictions
        string prediction_file = cmd.optionValue("predicted_labels");

        const std::array<int, 2> szCell = {1, 1};
        const std::array<int, 2> szBlock = {1, 6};
        const std::array<int, 2> hwStride = {1, 1};
        const auto margin = 6;
        const auto bins = 9;
        const bool sign = 1;
        auto params = TClassifierParams();
        params.C = 0.7;
        auto metric = sum_metric;
        ProblemParams pp =
            ProblemParams(szCell, szBlock, hwStride,
                          margin, bins, sign, params, metric);

        PredictData(data_file, model_file, prediction_file, pp);
    }

    return 0;
}
